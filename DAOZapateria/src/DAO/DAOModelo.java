package DAO;

import Entidades.Modelo;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOModelo {
    public List<Modelo> listarModelos()throws Exception{
        List<Modelo> listaModelos = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select mode.id_model, mode.descrip_model from modelo mode";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaModelos= new ArrayList<>();
            while(rs.next()){
                int id_model = rs.getInt("id_model");
                String descrip_model = rs.getString("descrip_model");

                Modelo tempModel = new Modelo(id_model, descrip_model);
                listaModelos.add(tempModel);
            }            
        } catch (Exception e) {
            throw e;
        }finally{
            if(rs != null && rs.isClosed() == false){
                rs.close();
            }
            rs=null;
            st=null;
            if(cn != null && cn.isClosed() == false){
                cn.close();
            }
            cn = null;
        }
        
        return listaModelos;
    }
}
