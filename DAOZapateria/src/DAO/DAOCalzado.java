package DAO;

import Entidades.Calzado;
import Entidades.Marca;
import Entidades.Modelo;
import Entidades.TipoCalzado;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOCalzado {

    public void registrarCalzado(Calzado calzado) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "INSERT INTO calzado(talla_calz, precioC_calz, precioV_calz, stock_calz, "
                + "stockMin_calz, color_calz, categ_calz, id_marca, id_model, id_tipoCalzado) "
                + "VALUES(" + calzado.getTalla_calz() + ", " + calzado.getPrecioC_calz() + ", "
                + "" + calzado.getPrecioV_calz() + ", " + calzado.getStock_calz() + ", "
                + "" + calzado.getStockMin_calz() + ", '" + calzado.getColor_calz() + "', "
                + "'" + calzado.getCateg_calz() + "', " + calzado.getMarca_calz().getId_marca()+ ", "
                + "" + calzado.getModel_calz().getId_model()+ ", " + calzado.getTipoCalzado_calz().getId_tipoCalzado()+ ")";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public List<Calzado> listarCalzados() throws Exception {
        List<Calzado> listaCalzados = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select cal.id_calz as id_calz, \n"
                + "cal.talla_calz as talla_calz, \n"
                + "cal.precioC_calz as precioC_calz, \n"
                + "cal.precioV_calz as precioV_calz, \n"
                + "cal.stock_calz as stock_calz, \n"
                + "cal.stockMin_calz as stockMin_calz, \n"
                + "cal.color_calz as color_calz, \n"
                + "cal.categ_calz as categ_calz, \n"
                + "ma.descrip_marca as marca_calz, \n"
                + "mo.descrip_model as model_calz,\n"
                + "tc.descrip_tipoCalzado as tipoCalzado_calz\n"
                + "from calzado cal \n"
                + "inner join marca ma on cal.id_marca = ma.id_marca\n"
                + "inner join modelo mo on cal.id_model = mo.id_model\n"
                + "inner join tipocalzado tc on cal.id_tipoCalzado = tc.id_tipoCalzado";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaCalzados = new ArrayList<>();
            while (rs.next()) {
                int id_calz = rs.getInt("id_calz");
                int talla_calz = rs.getInt("talla_calz");
                double precioC_calz = rs.getDouble("precioC_calz");
                double precioV_calz = rs.getDouble("precioV_calz");
                int stock_calz = rs.getInt("stock_calz");
                int stockMin_calz = rs.getInt("stockMin_calz");
                String color_calz = rs.getString("color_calz");
                String categ_calz = rs.getString("categ_calz");
                Marca marca_calz = new Marca();
                marca_calz.setDescrip_marca(rs.getString("marca_calz"));
                Modelo model_calz = new Modelo();
                model_calz.setDescrip_model(rs.getString("model_calz"));
                TipoCalzado tipoCalzado_calz = new TipoCalzado();
                tipoCalzado_calz.setDescrip_tipoCalzado(rs.getString("tipoCalzado_calz"));

                Calzado calzado = new Calzado(id_calz, talla_calz, precioC_calz, precioV_calz, stock_calz, stockMin_calz, color_calz, categ_calz, marca_calz, model_calz, tipoCalzado_calz);
//                calzado.setId_calz(id_calz);
//                calzado.setTalla_calz(talla_calz);
//                calzado.setPrecioC_calz(precioC_calz);
//                calzado.setPrecioV_calz(precioV_calz);
//                calzado.setStock_calz(stock_calz);
//                calzado.setStockMin_calz(stockMin_calz);
//                calzado.setColor_calz(color_calz);
//                calzado.setCateg_calz(categ_calz);
//                calzado.setMarca_calz(marca_calz);
//                calzado.setModel_calz(model_calz);
//                calzado.setTipoCalzado_calz(tipoCalzado_calz);
                
                listaCalzados.add(calzado);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return listaCalzados;
    }

    public void modificarCalzado(Calzado calzado) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "UPDATE calzado SET talla_calz=" + calzado.getTalla_calz() + ", precioC_calz=" + calzado.getPrecioC_calz() + ", "
                + "precioV_calz=" + calzado.getPrecioV_calz() + ", stockMin_calz=" + calzado.getStockMin_calz() + ", "
                + "color_calz='" + calzado.getColor_calz() + "', "
                + "categ_calz='" + calzado.getCateg_calz() + "', id_marca=" + calzado.getMarca_calz().getId_marca() + ", "
                + "id_model=" + calzado.getModel_calz().getId_model()+ ", id_tipoCalzado=" + calzado.getTipoCalzado_calz().getId_tipoCalzado()+ " "
                + "where id_calz=" + calzado.getId_calz();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Calzado obtenerCalzado(Calzado calzado) throws Exception {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select cal.talla_calz as talla_calz, \n"
                + "cal.precioC_calz as precioC_calz, \n"
                + "cal.precioV_calz as precioV_calz, \n"
                + "cal.stock_calz as stock_calz, \n"
                + "cal.stockMin_calz as stockMin_calz, \n"
                + "cal.color_calz as color_calz, \n"
                + "cal.categ_calz as categ_calz, \n"
                + "ma.descrip_marca as marca_calz, \n"
                + "mo.descrip_model as model_calz,\n"
                + "tc.descrip_tipoCalzado as tipoCalzado_calz\n"
                + "from calzado cal \n"
                + "inner join marca ma on cal.id_marca = ma.id_marca\n"
                + "inner join modelo mo on cal.id_model = mo.id_model\n"
                + "inner join tipocalzado tc on cal.id_tipoCalzado = tc.id_tipoCalzado where id_calz=" + calzado.getId_calz();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                int talla_calz = rs.getInt("talla_calz");
                double precioC_calz = rs.getDouble("precioC_calz");
                double precioV_calz = rs.getDouble("precioV_calz");
                int stock_calz = rs.getInt("stock_calz");
                int stockMin_calz = rs.getInt("stockMin_calz");
                String color_calz = rs.getString("color_calz");
                String categ_calz = rs.getString("categ_calz");
                Marca marca_calz = new Marca();
                marca_calz.setDescrip_marca(rs.getString("marca_calz"));
                Modelo model_calz = new Modelo();
                model_calz.setDescrip_model(rs.getString("model_calz"));
                TipoCalzado tipoCalzado_calz = new TipoCalzado();
                tipoCalzado_calz.setDescrip_tipoCalzado(rs.getString("tipoCalzado_calz"));
                
                calzado.setTalla_calz(talla_calz);
                calzado.setPrecioC_calz(precioC_calz);
                calzado.setPrecioV_calz(precioV_calz);
                calzado.setStock_calz(stock_calz);
                calzado.setStockMin_calz(stockMin_calz);
                calzado.setColor_calz(color_calz);
                calzado.setCateg_calz(categ_calz);
                calzado.setMarca_calz(marca_calz);
                calzado.setModel_calz(model_calz);
                calzado.setTipoCalzado_calz(tipoCalzado_calz);
                
                
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return calzado;
    }
}
