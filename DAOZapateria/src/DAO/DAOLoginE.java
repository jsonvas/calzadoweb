package DAO;

import java.sql.*;

public class DAOLoginE {
    public boolean validarLoginE(String id_loginE, String pass_loginE) throws Exception{
        boolean log = false;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select log.id_loginE, log.pass_loginE from logine log";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if(rs.next()){
                String user = rs.getString("id_loginE");
                String pass = rs.getString("pass_loginE");
                if(id_loginE.equals(user) && pass_loginE.equals(pass)){
                    log = true;
                }
            }            
        } catch (Exception e) {
            throw e;
        }finally{
            if(rs != null && rs.isClosed() == false){
                rs.close();
            }
            rs=null;
            st=null;
            if(cn != null && cn.isClosed() == false){
                cn.close();
            }
            cn = null;
        }
        return log;
    }
}
