package DAO;

import Entidades.Marca;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOMarca {
    public List<Marca> listarMarcas()throws Exception{
        List<Marca> listaMarcas = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select mar.id_marca, mar.descrip_marca from marca mar";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaMarcas= new ArrayList<>();
            while(rs.next()){
                int id_marca = rs.getInt("id_marca");
                String descrip_marca = rs.getString("descrip_marca");

                Marca tempMarca = new Marca(id_marca, descrip_marca);
                listaMarcas.add(tempMarca);
            }            
        } catch (Exception e) {
            throw e;
        }finally{
            if(rs != null && rs.isClosed() == false){
                rs.close();
            }
            rs=null;
            st=null;
            if(cn != null && cn.isClosed() == false){
                cn.close();
            }
            cn = null;
        }
        
        return listaMarcas;
    }
}
