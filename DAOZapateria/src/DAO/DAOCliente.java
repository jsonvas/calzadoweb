package DAO;

import Entidades.Cliente;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOCliente {

    public List<Cliente> listarClientes() throws Exception {
        List<Cliente> listaClientes = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select cli.id_cli, cli.tipodoc_cli, cli.ndoc_cli, cli.nom_cli, cli.apeP_cli, cli.apeM_cli, cli.telef_cli, cli.gen_cli, cli.fechaNac_cli, cli.email_cli from cliente cli";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaClientes = new ArrayList<>();
            while (rs.next()) {
                int id_cli = rs.getInt("id_cli");
                String tipodoc_cli = rs.getString("tipodoc_cli");
                String ndoc_cli = rs.getString("ndoc_cli");
                String nom_cli = rs.getString("nom_cli");
                String apeP_cli = rs.getString("apeP_cli");
                String apeM_cli = rs.getString("apeM_cli");
                String telef_cli = rs.getString("telef_cli");
                String gen_cli = rs.getString("gen_cli");
                String fechaNac_cli = rs.getString("fechaNac_cli");
                String email_cli = rs.getString("email_cli");

                Cliente tempCliente = new Cliente(id_cli, tipodoc_cli, ndoc_cli, nom_cli, apeP_cli, apeM_cli, telef_cli, gen_cli, fechaNac_cli, email_cli);
                listaClientes.add(tempCliente);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return listaClientes;
    }

    public void registrarCliente(Cliente cliente) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "INSERT INTO cliente(tipodoc_cli , ndoc_cli , nom_cli , apeP_cli , apeM_cli , telef_cli , gen_cli , fechaNac_cli , email_cli ) "
                + "VALUES( '" + cliente.getTipodoc_cli() + "', '" + cliente.getNdoc_cli() + "', '" + cliente.getNom_cli() + "', '" + cliente.getApeP_cli() + "', '" + cliente.getApeM_cli() + "', '" + cliente.getTelef_cli() + "', '" + cliente.getGen_cli() + "', '" + cliente.getFechaNac_cli() + "', '" + cliente.getEmail_cli() + "')";
        System.out.println(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Cliente obtenerCliente(Cliente cliente) throws Exception {
        Cliente clie = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select cli.id_cli as id_cli , cli.tipodoc_cli as tipodoc_cli , cli.ndoc_cli as ndoc_cli , cli.nom_cli as nom_cli , cli.apeP_cli as apeP_cli , cli.apeM_cli as apeM_cli , cli.telef_cli as telef_cli , cli.gen_cli as gen_cli , cli.fechaNac_cli as fechaNac_cli , cli.email_cli as email_cli from cliente cli where id_cli=" + cliente.getId_cli();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            cliente = new Cliente();
            while (rs.next()) {
                int id_cli = Integer.parseInt(rs.getString("id_cli"));
                String tipodoc_cli = rs.getString("tipodoc_cli");
                String ndoc_cli = rs.getString("ndoc_cli");
                String nom_cli = rs.getString("nom_cli");
                String apeP_cli = rs.getString("apeP_cli");
                String apeM_cli = rs.getString("apeM_cli");
                String telef_cli = rs.getString("telef_cli");
                String gen_cli = rs.getString("gen_cli");
                String fechaNac_cli = rs.getString("fechaNac_cli");
                String email_cli = rs.getString("email_cli");
                clie = new Cliente(id_cli, tipodoc_cli, ndoc_cli, nom_cli, apeP_cli, apeM_cli, telef_cli, gen_cli, fechaNac_cli, email_cli);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return clie;
    }

    public void modificarCliente(Cliente cliente) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "UPDATE cliente SET tipodoc_cli ='" + cliente.getTipodoc_cli() + "', ndoc_cli ='" + cliente.getNdoc_cli() + "', nom_cli ='" + cliente.getNom_cli() + "', apeP_cli ='" + cliente.getApeP_cli() + "', apeM_cli ='" + cliente.getApeM_cli() + "', telef_cli ='" + cliente.getTelef_cli() + "', gen_cli ='" + cliente.getGen_cli() + "', fechaNac_cli ='" + cliente.getFechaNac_cli() + "', email_cli ='" + cliente.getEmail_cli() + "' where id_cli="+ cliente.getId_cli();
        System.out.print(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
}
