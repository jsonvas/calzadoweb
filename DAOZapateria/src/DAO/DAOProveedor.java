package DAO;

import Entidades.Proveedor;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOProveedor {

    public List listarProveedores() throws Exception {
        List listaProveedores = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select pro.id_prov as id_prov , pro.nom_prov as nom_prov , pro.tipdoc_prov as tipdoc_prov , pro.ndoc_prov as ndoc_prov , pro.telef_prov as telef_prov , pro.direc_prov as direc_prov , pro.email_prov as email_prov , pro.estado_prov as estado_prov from proveedor pro";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaProveedores = new ArrayList<>();
            while (rs.next()) {
                int id_prov = rs.getInt("id_prov");
                String nom_prov = rs.getString("nom_prov");
                String tipdoc_prov = rs.getString("tipdoc_prov");
                String ndoc_prov = rs.getString("ndoc_prov");
                String telef_prov = rs.getString("telef_prov");
                String direc_prov = rs.getString("direc_prov");
                String email_prov = rs.getString("email_prov");
                boolean estado_prov = rs.getBoolean("estado_prov");
                Proveedor proveedor = new Proveedor(id_prov, nom_prov, tipdoc_prov, ndoc_prov, telef_prov, direc_prov, email_prov, estado_prov);
                listaProveedores.add(proveedor);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return listaProveedores;
    }

    public void registrarProveedor(Proveedor proveedor) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "INSERT INTO proveedor(nom_prov, tipdoc_prov, ndoc_prov, telef_prov, direc_prov, email_prov, estado_prov)"
                + "VALUES('" + proveedor.getNom_prov() + "', '" + proveedor.getTipdoc_prov() + "', "
                + "'" + proveedor.getNdoc_prov() + "', '" + proveedor.getTelef_prov() + "', "
                + "'" + proveedor.getDirec_prov() + "', '" + proveedor.getEmail_prov() + "', "
                + "" + proveedor.getEstado_prov() + ")";
        System.out.println(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Proveedor obtenerProveedor(Proveedor proveedor) throws Exception {
        Proveedor prov = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select prov.id_prov, prov.nom_prov, prov.tipdoc_prov, prov.ndoc_prov, prov.telef_prov,"
                + " prov.direc_prov, prov.email_prov, prov.estado_prov from proveedor prov where id_prov=" + proveedor.getId_prov();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            prov = new Proveedor();
            while (rs.next()) {
                int id_prov = rs.getInt("id_prov");
                String nom_prov = rs.getString("nom_prov");
                String tipdoc_prov = rs.getString("tipdoc_prov");
                String ndoc_prov = rs.getString("ndoc_prov");
                String telef_prov = rs.getString("telef_prov");
                String direc_prov = rs.getString("direc_prov");
                String email_prov = rs.getString("email_prov");
                boolean estado_prov = rs.getBoolean("estado_prov");

                prov = new Proveedor(id_prov, nom_prov, tipdoc_prov, ndoc_prov, telef_prov, direc_prov, email_prov, estado_prov);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }

        return prov;
    }

    public void modificarProveedor(Proveedor proveedor) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "UPDATE proveedor SET nom_prov='" + proveedor.getNom_prov() + "', tipdoc_prov='" + proveedor.getTipdoc_prov() + "', "
                + "ndoc_prov='" + proveedor.getNdoc_prov() + "', telef_prov='" + proveedor.getTelef_prov() + "', "
                + "direc_prov='" + proveedor.getDirec_prov() + "', "
                + "email_prov='" + proveedor.getEmail_prov() + "', estado_prov=" + proveedor.getEstado_prov()+" where id_prov="+proveedor.getId_prov();
        System.out.print(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    
}
