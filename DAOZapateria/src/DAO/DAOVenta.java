package DAO;

import Entidades.Cliente;
import Entidades.Empleado;
import Entidades.Venta;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOVenta {

    public List<Venta> listarVentas() throws Exception {
        List<Venta> listaVentas = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select ven.id_vent as id_vent , ven.fecha_vent as fecha_vent , ven.total_vent as total_vent , ven.tipo_vent as tipo_vent , ven.tipoPago_vent as tipoPago_vent , ven.id_emple as id_emple , ven.id_clie as id_clie , ven.estado_vent as estado_vent from venta ven";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaVentas = new ArrayList<>();
            while (rs.next()) {
                int id_vent = rs.getInt("id_vent");
                String fecha_vent = rs.getString("fecha_vent");
                double total_vent = rs.getDouble("total_vent");
                String tipo_vent = rs.getString("tipo_vent");
                String tipoPago_vent = rs.getString("tipoPago_vent");
                Empleado emple_vent = new Empleado();
                emple_vent.setId_emp(rs.getInt("id_emple"));
                Cliente clie_vent = new Cliente();
                clie_vent.setId_cli(rs.getInt("id_clie"));
                boolean estado_vent = rs.getBoolean("estado_vent");
                Venta venta = new Venta(id_vent, fecha_vent, total_vent, tipo_vent, tipoPago_vent, emple_vent, clie_vent, estado_vent);
                listaVentas.add(venta);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return listaVentas;
    }

    public void registrarVenta(Venta venta) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = " INSERT INTO venta( fecha_vent , total_vent , tipo_vent , tipoPago_vent , id_emple , id_clie , estado_vent ) VALUES( '" + venta.getFecha_vent() + "', '" + venta.getTotal_vent() + "', '" + venta.getTipo_vent() + "', '" + venta.getTipoPago_vent() + "', '" + venta.getEmple_vent().getId_emp()+ "', '" + venta.getClie_vent().getId_cli()+ "', " + venta.isEstado_vent() + ")";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Venta obtenerVenta(Venta venta) throws Exception {
        Venta vent = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select ven.id_vent as id_vent , ven.fecha_vent as fecha_vent , ven.total_vent as total_vent , ven.tipo_vent as tipo_vent , ven.tipoPago_vent as tipoPago_vent , ven.id_emple as id_emple , ven.id_clie as id_clie , ven.estado_vent as estado_vent from venta ven where id_vent=" + venta.getId_vent();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            vent = new Venta();
            if (rs.next()) {
                int id_vent = rs.getInt("id_vent");
                String fecha_vent = rs.getString("fecha_vent");
                double total_vent = rs.getDouble("total_vent");
                String tipo_vent = rs.getString("tipo_vent");
                String tipoPago_vent = rs.getString("tipoPago_vent");
                Empleado emple_vent = new Empleado();
                emple_vent.setId_emp(rs.getInt("id_emple"));
                Cliente clie_vent = new Cliente();
                clie_vent.setId_cli(rs.getInt("id_clie"));
                boolean estado_vent = rs.getBoolean("estado_vent");
                vent = new Venta(id_vent, fecha_vent, total_vent, tipo_vent, tipoPago_vent, emple_vent, clie_vent, estado_vent);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return vent;
    }

    public void modificarVenta(Venta venta) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "UPDATE venta SET fecha_vent ='" + venta.getFecha_vent() + "', total_vent ='" + venta.getTotal_vent() + "', tipo_vent ='" + venta.getTipo_vent() + "', tipoPago_vent ='" + venta.getTipoPago_vent() + "', id_emple ='" + venta.getEmple_vent().getId_emp()+ "', id_clie ='" + venta.getClie_vent().getId_cli()+ "', estado_vent ='" + venta.isEstado_vent() + "' where id_vent=" + venta.getId_vent();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
}
