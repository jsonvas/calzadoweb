package DAO;

import Entidades.Calzado;
import Entidades.Dventa;
import Entidades.Venta;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAODVenta {

    public List<Dventa> listarDventas(Dventa dventa) throws Exception {
        List<Dventa> listaDventas = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select dve.id_dvent as id_dvent , dve.id_vent as id_vent , dve.id_calz as id_calz from dventa dve where id_dvent=" + dventa.getId_dvent();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaDventas = new ArrayList<>();
            while (rs.next()) {
                int id_dvent = rs.getInt("id_dvent");
                Venta vent_dvent = new Venta();
                vent_dvent.setId_vent(rs.getInt("id_vent"));
                Calzado calz_dvent = new Calzado();
                calz_dvent.setId_calz(rs.getInt("id_calz"));
                Dventa dvent = new Dventa(id_dvent, vent_dvent, calz_dvent);
                listaDventas.add(dvent);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return listaDventas;
    }

    public void registrarDventa(Dventa dventa) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = " INSERT INTO dventa( id_vent , id_calz ) VALUES( '" + dventa.getVent_dvent().getId_vent()+ "', '" + dventa.getCalz_dvent().getId_calz()+ "')";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Dventa obtenerDventa(Dventa dventa) throws Exception {
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select dve.id_dvent as id_dvent , dve.id_vent as id_vent , dve.id_calz as id_calz from dventa dve where id_dvent=" + dventa.getId_dvent();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            if (rs.next()) {
                Venta vent_dvent = new Venta();
                vent_dvent.setId_vent(rs.getInt("id_vent"));
                Calzado calz_dvent = new Calzado();
                calz_dvent.setId_calz(rs.getInt("id_calz"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return dventa;
    }

//    public void modificarDventa(Dventa dventa) throws Exception {
//        Connection cn = null;
//        Statement st = null;
//        String sql = "UPDATE dventa SET id_vent ='" + dventa.getId_vent() + "', id_calz ='" + dventa.getId_calz() + "' where id_dvent=" + dventa.getId_dvent();
//        System.out.print(sql);
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
//            st = cn.createStatement();
//            st.executeUpdate(sql);
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            st = null;
//            if (cn != null && cn.isClosed() == false) {
//                cn.close();
//            }
//            cn = null;
//        }
//    }
}
