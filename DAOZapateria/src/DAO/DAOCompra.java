package DAO;

import Entidades.Compra;
import Entidades.Proveedor;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOCompra {

    public List<Compra> listarCompras() throws Exception {
        List<Compra> listaCompras = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select com.id_comp as id_comp , com.fecha_comp as fecha_comp , com.total_comp as total_comp , com.id_prov as id_prov from compra com";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaCompras = new ArrayList<>();
            while (rs.next()) {
                int id_comp = rs.getInt("id_comp");
                String fecha_comp = rs.getString("fecha_comp");
                double total_comp = rs.getDouble("total_comp");
                Proveedor prov_comp = new Proveedor();
                prov_comp.setId_prov(rs.getInt("id_prov"));
                Compra compra = new Compra(id_comp, fecha_comp, total_comp, prov_comp);
                listaCompras.add(compra);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return listaCompras;
    }

    public void registrarCompra(Compra compra) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "INSERT INTO compra( fecha_comp , total_comp , id_prov ) VALUES( '" + compra.getFecha_comp() + "', '" + compra.getTotal_comp() + "', '" + compra.getProv_comp().getId_prov()+ "')";
        System.out.println(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }

    public Compra obtenerCompra(Compra compra) throws Exception {
        Compra comp = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select com.id_comp as id_comp , com.fecha_comp as fecha_comp , com.total_comp as total_comp , com.id_prov as id_prov from compra com where id_comp=" + compra.getId_comp();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            comp = new Compra();
            if (rs.next()) {
                int id_comp = rs.getInt("id_comp");
                String fecha_comp = rs.getString("fecha_comp");
                double total_comp = rs.getDouble("total_comp");
                Proveedor prov_comp = new Proveedor();
                prov_comp.setId_prov(rs.getInt("id_prov"));
                comp = new Compra(id_comp, fecha_comp, total_comp, prov_comp);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (rs != null && rs.isClosed() == false) {
                rs.close();
            }
            rs = null;
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
        return comp;
    }

    public void modificarCompra(Compra compra) throws Exception {
        Connection cn = null;
        Statement st = null;
        String sql = "UPDATE compra SET fecha_comp ='" + compra.getFecha_comp() + "', total_comp ='" + compra.getTotal_comp() + "', id_prov ='" + compra.getProv_comp().getId_prov()+ "' where id_comp=" + compra.getId_comp();
        System.out.print(sql);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            st.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        } finally {
            st = null;
            if (cn != null && cn.isClosed() == false) {
                cn.close();
            }
            cn = null;
        }
    }
}
