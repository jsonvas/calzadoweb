package DAO;

import Entidades.TipoCalzado;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAOTipoCalzado {
    public List<TipoCalzado> listarTipoCalzados()throws Exception{
        List<TipoCalzado> listaTipoCalzados = null;
        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = "select tip.id_tipoCalzado, tip.descrip_tipoCalzado from tipocalzado tip";
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/" + "db_zapateria?user=root&password");
            st = cn.createStatement();
            rs = st.executeQuery(sql);
            listaTipoCalzados= new ArrayList<>();
            while(rs.next()){
                int id_tipoCalzado = rs.getInt("id_tipoCalzado");
                String descrip_tipoCalzado = rs.getString("descrip_tipoCalzado");

                TipoCalzado tempTipoCalzado = new TipoCalzado(id_tipoCalzado, descrip_tipoCalzado);
                listaTipoCalzados.add(tempTipoCalzado);
            }            
        } catch (Exception e) {
            throw e;
        }finally{
            if(rs != null && rs.isClosed() == false){
                rs.close();
            }
            rs=null;
            st=null;
            if(cn != null && cn.isClosed() == false){
                cn.close();
            }
            cn = null;
        }
        
        return listaTipoCalzados;
    }
}
