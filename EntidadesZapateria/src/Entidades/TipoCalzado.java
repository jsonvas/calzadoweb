package Entidades;

public class TipoCalzado {
    private int id_tipoCalzado;
    private String descrip_tipoCalzado;

    public TipoCalzado() {
    }

    public TipoCalzado(int id_tipoCalzado, String descrip_tipoCalzado) {
        this.id_tipoCalzado = id_tipoCalzado;
        this.descrip_tipoCalzado = descrip_tipoCalzado;
    }
    
    

    public int getId_tipoCalzado() {
        return id_tipoCalzado;
    }

    public void setId_tipoCalzado(int id_tipoCalzado) {
        this.id_tipoCalzado = id_tipoCalzado;
    }

    public String getDescrip_tipoCalzado() {
        return descrip_tipoCalzado;
    }

    public void setDescrip_tipoCalzado(String descrip_tipoCalzado) {
        this.descrip_tipoCalzado = descrip_tipoCalzado;
    }
    
    
    
}
