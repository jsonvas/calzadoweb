package Entidades;

public class Proveedor {
    private int id_prov;
    private String nom_prov;
    private String tipdoc_prov;
    private String ndoc_prov;
    private String telef_prov;
    private String direc_prov;
    private String email_prov;
    private boolean estado_prov;

    public Proveedor() {
    }

    public Proveedor(String nom_prov, String tipdoc_prov, String ndoc_prov, String telef_prov, String direc_prov, String email_prov, boolean estado_prov) {
        this.nom_prov = nom_prov;
        this.tipdoc_prov = tipdoc_prov;
        this.ndoc_prov = ndoc_prov;
        this.telef_prov = telef_prov;
        this.direc_prov = direc_prov;
        this.email_prov = email_prov;
        this.estado_prov = estado_prov;
    }
    

    public Proveedor(int id_prov, String nom_prov, String tipdoc_prov, String ndoc_prov, String telef_prov, String direc_prov, String email_prov, boolean estado_prov) {
        this.id_prov = id_prov;
        this.nom_prov = nom_prov;
        this.tipdoc_prov = tipdoc_prov;
        this.ndoc_prov = ndoc_prov;
        this.telef_prov = telef_prov;
        this.direc_prov = direc_prov;
        this.email_prov = email_prov;
        this.estado_prov = estado_prov;
    }

    public int getId_prov() {
        return id_prov;
    }

    public void setId_prov(int id_prov) {
        this.id_prov = id_prov;
    }

    public String getNom_prov() {
        return nom_prov;
    }

    public void setNom_prov(String nom_prov) {
        this.nom_prov = nom_prov;
    }

    public String getTipdoc_prov() {
        return tipdoc_prov;
    }

    public void setTipdoc_prov(String tipdoc_prov) {
        this.tipdoc_prov = tipdoc_prov;
    }

    public String getNdoc_prov() {
        return ndoc_prov;
    }

    public void setNdoc_prov(String ndoc_prov) {
        this.ndoc_prov = ndoc_prov;
    }

    public String getTelef_prov() {
        return telef_prov;
    }

    public void setTelef_prov(String telef_prov) {
        this.telef_prov = telef_prov;
    }

    public String getDirec_prov() {
        return direc_prov;
    }

    public void setDirec_prov(String direc_prov) {
        this.direc_prov = direc_prov;
    }

    public String getEmail_prov() {
        return email_prov;
    }

    public void setEmail_prov(String email_prov) {
        this.email_prov = email_prov;
    }

    public boolean getEstado_prov() {
        return estado_prov;
    }

    public void setEstado_prov(boolean estado_prov) {
        this.estado_prov = estado_prov;
    }
    
    
    
}
