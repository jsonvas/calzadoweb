package Entidades;

public class Venta {
    private int id_vent;
    private String fecha_vent;
    private double total_vent;
    private String tipo_vent;
    private String tipoPago_vent;
    private Empleado emple_vent;
    private Cliente clie_vent;
    private boolean estado_vent;

    public Venta() {
        
    }

    public Venta(int id_vent, String fecha_vent, double total_vent, String tipo_vent, String tipoPago_vent, Empleado emple_vent, Cliente clie_vent, boolean estado_vent) {
        this.id_vent = id_vent;
        this.fecha_vent = fecha_vent;
        this.total_vent = total_vent;
        this.tipo_vent = tipo_vent;
        this.tipoPago_vent = tipoPago_vent;
        this.emple_vent = emple_vent;
        this.clie_vent = clie_vent;
        this.estado_vent = estado_vent;
    }

    public Venta(String fecha_vent, double total_vent, String tipo_vent, String tipoPago_vent, Empleado emple_vent, Cliente clie_vent, boolean estado_vent) {
        this.fecha_vent = fecha_vent;
        this.total_vent = total_vent;
        this.tipo_vent = tipo_vent;
        this.tipoPago_vent = tipoPago_vent;
        this.emple_vent = emple_vent;
        this.clie_vent = clie_vent;
        this.estado_vent = estado_vent;
    }

    public int getId_vent() {
        return id_vent;
    }

    public void setId_vent(int id_vent) {
        this.id_vent = id_vent;
    }

    public String getFecha_vent() {
        return fecha_vent;
    }

    public void setFecha_vent(String fecha_vent) {
        this.fecha_vent = fecha_vent;
    }

    public double getTotal_vent() {
        return total_vent;
    }

    public void setTotal_vent(double total_vent) {
        this.total_vent = total_vent;
    }

    public String getTipo_vent() {
        return tipo_vent;
    }

    public void setTipo_vent(String tipo_vent) {
        this.tipo_vent = tipo_vent;
    }

    public String getTipoPago_vent() {
        return tipoPago_vent;
    }

    public void setTipoPago_vent(String tipoPago_vent) {
        this.tipoPago_vent = tipoPago_vent;
    }

    public Empleado getEmple_vent() {
        return emple_vent;
    }

    public void setEmple_vent(Empleado emple_vent) {
        this.emple_vent = emple_vent;
    }

    public Cliente getClie_vent() {
        return clie_vent;
    }

    public void setClie_vent(Cliente clie_vent) {
        this.clie_vent = clie_vent;
    }

    public boolean isEstado_vent() {
        return estado_vent;
    }

    public void setEstado_vent(boolean estado_vent) {
        this.estado_vent = estado_vent;
    }
    
    
    
    
}
