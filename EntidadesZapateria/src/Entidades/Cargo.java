package Entidades;

public class Cargo {
    private int id_cargo;
    private String nom_cargo;

    public Cargo() {
    }

    public Cargo(int id_cargo, String nom_cargo) {
        this.id_cargo = id_cargo;
        this.nom_cargo = nom_cargo;
    }

    public Cargo(String nom_cargo) {
        this.nom_cargo = nom_cargo;
    }

    public int getId_cargo() {
        return id_cargo;
    }

    public void setId_cargo(int id_cargo) {
        this.id_cargo = id_cargo;
    }

    public String getNom_cargo() {
        return nom_cargo;
    }

    public void setNom_cargo(String nom_cargo) {
        this.nom_cargo = nom_cargo;
    }
    
    
    
}
