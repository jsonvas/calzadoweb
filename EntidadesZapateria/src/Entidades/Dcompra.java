package Entidades;

public class Dcompra {
    private int id_dcomp;
    private Compra comp_dcomp;
    private Calzado calza_dcomp;

    public Dcompra() {
    }

    public Dcompra(int id_dcomp, Compra comp_dcomp, Calzado calza_dcomp) {
        this.id_dcomp = id_dcomp;
        this.comp_dcomp = comp_dcomp;
        this.calza_dcomp = calza_dcomp;
    }

    public Dcompra(Compra comp_dcomp, Calzado calza_dcomp) {
        this.comp_dcomp = comp_dcomp;
        this.calza_dcomp = calza_dcomp;
    }

    public int getId_dcomp() {
        return id_dcomp;
    }

    public void setId_dcomp(int id_dcomp) {
        this.id_dcomp = id_dcomp;
    }

    public Compra getComp_dcomp() {
        return comp_dcomp;
    }

    public void setComp_dcomp(Compra comp_dcomp) {
        this.comp_dcomp = comp_dcomp;
    }

    public Calzado getCalza_dcomp() {
        return calza_dcomp;
    }

    public void setCalza_dcomp(Calzado calza_dcomp) {
        this.calza_dcomp = calza_dcomp;
    }

    
    
}
