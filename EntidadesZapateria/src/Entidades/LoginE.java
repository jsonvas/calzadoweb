package Entidades;

public class LoginE {
    String id_loginE;
    String pass_loginE;
    int id_emp;

    public LoginE() {
    }

    public LoginE(String id_loginE, String pass_loginE, int id_emp) {
        this.id_loginE = id_loginE;
        this.pass_loginE = pass_loginE;
        this.id_emp = id_emp;
    }

    public String getId_loginE() {
        return id_loginE;
    }

    public void setId_loginE(String id_loginE) {
        this.id_loginE = id_loginE;
    }

    public String getPass_loginE() {
        return pass_loginE;
    }

    public void setPass_loginE(String pass_loginE) {
        this.pass_loginE = pass_loginE;
    }

    public int getId_emp() {
        return id_emp;
    }

    public void setId_emp(int id_emp) {
        this.id_emp = id_emp;
    }
    
    
}
