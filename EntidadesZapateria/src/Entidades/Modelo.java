package Entidades;

public class Modelo {
    private int id_model;
    private String descrip_model;

    public Modelo() {
    }

    public Modelo(int id_model, String descrip_model) {
        this.id_model = id_model;
        this.descrip_model = descrip_model;
    }

    public int getId_model() {
        return id_model;
    }

    public void setId_model(int id_model) {
        this.id_model = id_model;
    }

    public String getDescrip_model() {
        return descrip_model;
    }

    public void setDescrip_model(String descrip_model) {
        this.descrip_model = descrip_model;
    }
    
    
    
}
