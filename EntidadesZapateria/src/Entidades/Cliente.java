package Entidades;

public class Cliente {
    private int id_cli;
    private String tipodoc_cli;
    private String ndoc_cli;
    private String nom_cli;
    private String apeP_cli;
    private String apeM_cli;
    private String telef_cli;
    private String gen_cli;
    private String fechaNac_cli;
    private String email_cli;

    public Cliente() {
    }

    public Cliente(String tipodoc_cli, String ndoc_cli, String nom_cli, String apeP_cli, String apeM_cli, String telef_cli, String gen_cli, String fechaNac_cli, String email_cli) {
        this.tipodoc_cli = tipodoc_cli;
        this.ndoc_cli = ndoc_cli;
        this.nom_cli = nom_cli;
        this.apeP_cli = apeP_cli;
        this.apeM_cli = apeM_cli;
        this.telef_cli = telef_cli;
        this.gen_cli = gen_cli;
        this.fechaNac_cli = fechaNac_cli;
        this.email_cli = email_cli;
    }
    

    public Cliente(int id_cli, String tipodoc_cli, String ndoc_cli, String nom_cli, String apeP_cli, String apeM_cli, String telef_cli, String gen_cli, String fechaNac_cli, String email_cli) {
        this.id_cli = id_cli;
        this.tipodoc_cli = tipodoc_cli;
        this.ndoc_cli = ndoc_cli;
        this.nom_cli = nom_cli;
        this.apeP_cli = apeP_cli;
        this.apeM_cli = apeM_cli;
        this.telef_cli = telef_cli;
        this.gen_cli = gen_cli;
        this.fechaNac_cli = fechaNac_cli;
        this.email_cli = email_cli;
    }

    public int getId_cli() {
        return id_cli;
    }

    public void setId_cli(int id_cli) {
        this.id_cli = id_cli;
    }

    public String getTipodoc_cli() {
        return tipodoc_cli;
    }

    public void setTipodoc_cli(String tipodoc_cli) {
        this.tipodoc_cli = tipodoc_cli;
    }

    public String getNdoc_cli() {
        return ndoc_cli;
    }

    public void setNdoc_cli(String ndoc_cli) {
        this.ndoc_cli = ndoc_cli;
    }

    public String getNom_cli() {
        return nom_cli;
    }

    public void setNom_cli(String nom_cli) {
        this.nom_cli = nom_cli;
    }

    public String getApeP_cli() {
        return apeP_cli;
    }

    public void setApeP_cli(String apeP_cli) {
        this.apeP_cli = apeP_cli;
    }

    public String getApeM_cli() {
        return apeM_cli;
    }

    public void setApeM_cli(String apeM_cli) {
        this.apeM_cli = apeM_cli;
    }

    public String getTelef_cli() {
        return telef_cli;
    }

    public void setTelef_cli(String telef_cli) {
        this.telef_cli = telef_cli;
    }

    public String getGen_cli() {
        return gen_cli;
    }

    public void setGen_cli(String gen_cli) {
        this.gen_cli = gen_cli;
    }

    public String getFechaNac_cli() {
        return fechaNac_cli;
    }

    public void setFechaNac_cli(String fechaNac_cli) {
        this.fechaNac_cli = fechaNac_cli;
    }

    public String getEmail_cli() {
        return email_cli;
    }

    public void setEmail_cli(String email_cli) {
        this.email_cli = email_cli;
    }

    
}
