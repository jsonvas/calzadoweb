package Entidades;

public class Compra {
    private int id_comp;
    private String fecha_comp;
    private double total_comp;
    private Proveedor prov_comp;

    public Compra() {
    }

    public Compra(int id_comp, String fecha_comp, double total_comp, Proveedor prov_comp) {
        this.id_comp = id_comp;
        this.fecha_comp = fecha_comp;
        this.total_comp = total_comp;
        this.prov_comp = prov_comp;
    }

    public Compra(String fecha_comp, double total_comp, Proveedor prov_comp) {
        this.fecha_comp = fecha_comp;
        this.total_comp = total_comp;
        this.prov_comp = prov_comp;
    }

    public int getId_comp() {
        return id_comp;
    }

    public void setId_comp(int id_comp) {
        this.id_comp = id_comp;
    }

    public String getFecha_comp() {
        return fecha_comp;
    }

    public void setFecha_comp(String fecha_comp) {
        this.fecha_comp = fecha_comp;
    }

    public double getTotal_comp() {
        return total_comp;
    }

    public void setTotal_comp(double total_comp) {
        this.total_comp = total_comp;
    }

    public Proveedor getProv_comp() {
        return prov_comp;
    }

    public void setProv_comp(Proveedor prov_comp) {
        this.prov_comp = prov_comp;
    }

    
    
}
