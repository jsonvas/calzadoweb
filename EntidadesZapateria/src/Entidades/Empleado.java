package Entidades;

public class Empleado {
    private int id_emp;
    private String dni_emp;
    private String nom_emp;
    private String apeP_emp;
    private String apeM_emp;
    private String fechaNac_emp;
    private String email_emp;
    private String telef_emp;
    private String direc_emp;
    private double sueldo_emp;
    private Cargo cargo_emp;

    public Empleado() {
    }

    public Empleado(int id_emp, String dni_emp, String nom_emp, String apeP_emp, String apeM_emp, String fechaNac_emp, String email_emp, String telef_emp, String direc_emp, double sueldo_emp, Cargo cargo_emp) {
        this.id_emp = id_emp;
        this.dni_emp = dni_emp;
        this.nom_emp = nom_emp;
        this.apeP_emp = apeP_emp;
        this.apeM_emp = apeM_emp;
        this.fechaNac_emp = fechaNac_emp;
        this.email_emp = email_emp;
        this.telef_emp = telef_emp;
        this.direc_emp = direc_emp;
        this.sueldo_emp = sueldo_emp;
        this.cargo_emp = cargo_emp;
    }

    public Empleado(String dni_emp, String nom_emp, String apeP_emp, String apeM_emp, String fechaNac_emp, String email_emp, String telef_emp, String direc_emp, double sueldo_emp, Cargo cargo_emp) {
        this.dni_emp = dni_emp;
        this.nom_emp = nom_emp;
        this.apeP_emp = apeP_emp;
        this.apeM_emp = apeM_emp;
        this.fechaNac_emp = fechaNac_emp;
        this.email_emp = email_emp;
        this.telef_emp = telef_emp;
        this.direc_emp = direc_emp;
        this.sueldo_emp = sueldo_emp;
        this.cargo_emp = cargo_emp;
    }

    public int getId_emp() {
        return id_emp;
    }

    public void setId_emp(int id_emp) {
        this.id_emp = id_emp;
    }

    public String getDni_emp() {
        return dni_emp;
    }

    public void setDni_emp(String dni_emp) {
        this.dni_emp = dni_emp;
    }

    public String getNom_emp() {
        return nom_emp;
    }

    public void setNom_emp(String nom_emp) {
        this.nom_emp = nom_emp;
    }

    public String getApeP_emp() {
        return apeP_emp;
    }

    public void setApeP_emp(String apeP_emp) {
        this.apeP_emp = apeP_emp;
    }

    public String getApeM_emp() {
        return apeM_emp;
    }

    public void setApeM_emp(String apeM_emp) {
        this.apeM_emp = apeM_emp;
    }

    public String getFechaNac_emp() {
        return fechaNac_emp;
    }

    public void setFechaNac_emp(String fechaNac_emp) {
        this.fechaNac_emp = fechaNac_emp;
    }

    public String getEmail_emp() {
        return email_emp;
    }

    public void setEmail_emp(String email_emp) {
        this.email_emp = email_emp;
    }

    public String getTelef_emp() {
        return telef_emp;
    }

    public void setTelef_emp(String telef_emp) {
        this.telef_emp = telef_emp;
    }

    public String getDirec_emp() {
        return direc_emp;
    }

    public void setDirec_emp(String direc_emp) {
        this.direc_emp = direc_emp;
    }

    public double getSueldo_emp() {
        return sueldo_emp;
    }

    public void setSueldo_emp(double sueldo_emp) {
        this.sueldo_emp = sueldo_emp;
    }

    public Cargo getCargo_emp() {
        return cargo_emp;
    }

    public void setCargo_emp(Cargo cargo_emp) {
        this.cargo_emp = cargo_emp;
    }
    
    
    
}