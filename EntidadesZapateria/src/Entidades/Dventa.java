package Entidades;

public class Dventa {
   private int id_dvent;
   private Venta vent_dvent;
   private Calzado calz_dvent;

    public Dventa() {
    }

    public Dventa(int id_dvent, Venta vent_dvent, Calzado calz_dvent) {
        this.id_dvent = id_dvent;
        this.vent_dvent = vent_dvent;
        this.calz_dvent = calz_dvent;
    }

    public Dventa(Venta vent_dvent, Calzado calz_dvent) {
        this.vent_dvent = vent_dvent;
        this.calz_dvent = calz_dvent;
    }

    public int getId_dvent() {
        return id_dvent;
    }

    public void setId_dvent(int id_dvent) {
        this.id_dvent = id_dvent;
    }

    public Venta getVent_dvent() {
        return vent_dvent;
    }

    public void setVent_dvent(Venta vent_dvent) {
        this.vent_dvent = vent_dvent;
    }

    public Calzado getCalz_dvent() {
        return calz_dvent;
    }

    public void setCalz_dvent(Calzado calz_dvent) {
        this.calz_dvent = calz_dvent;
    }

    
   
}
