package Entidades;

public class Calzado {
    private int id_calz;
    private int talla_calz;
    private double precioC_calz; 
    private double precioV_calz;
    private int stock_calz;
    private int stockMin_calz;
    private String color_calz;
    private String categ_calz;
    
    private Marca marca_calz;
    private Modelo model_calz;
    private TipoCalzado tipoCalzado_calz;
    

    public Calzado() {
    }

    public Calzado(int id_calz, int talla_calz, double precioC_calz, double precioV_calz, int stock_calz, int stockMin_calz, String color_calz, String categ_calz, Marca marca_calz, Modelo model_calz, TipoCalzado tipoCalzado_calz) {
        this.id_calz = id_calz;
        this.talla_calz = talla_calz;
        this.precioC_calz = precioC_calz;
        this.precioV_calz = precioV_calz;
        this.stock_calz = stock_calz;
        this.stockMin_calz = stockMin_calz;
        this.color_calz = color_calz;
        this.categ_calz = categ_calz;
        this.marca_calz = marca_calz;
        this.model_calz = model_calz;
        this.tipoCalzado_calz = tipoCalzado_calz;
    }

    public Calzado(int talla_calz, double precioC_calz, double precioV_calz, int stock_calz, int stockMin_calz, String color_calz, String categ_calz, Marca marca_calz, Modelo model_calz, TipoCalzado tipoCalzado_calz) {
        this.talla_calz = talla_calz;
        this.precioC_calz = precioC_calz;
        this.precioV_calz = precioV_calz;
        this.stock_calz = stock_calz;
        this.stockMin_calz = stockMin_calz;
        this.color_calz = color_calz;
        this.categ_calz = categ_calz;
        this.marca_calz = marca_calz;
        this.model_calz = model_calz;
        this.tipoCalzado_calz = tipoCalzado_calz;
    }

    

    public int getId_calz() {
        return id_calz;
    }

    public void setId_calz(int id_calz) {
        this.id_calz = id_calz;
    }

    public int getTalla_calz() {
        return talla_calz;
    }

    public void setTalla_calz(int talla_calz) {
        this.talla_calz = talla_calz;
    }

    public double getPrecioC_calz() {
        return precioC_calz;
    }

    public void setPrecioC_calz(double precioC_calz) {
        this.precioC_calz = precioC_calz;
    }

    public double getPrecioV_calz() {
        return precioV_calz;
    }

    public void setPrecioV_calz(double precioV_calz) {
        this.precioV_calz = precioV_calz;
    }

    public int getStock_calz() {
        return stock_calz;
    }

    public void setStock_calz(int stock_calz) {
        this.stock_calz = stock_calz;
    }

    public int getStockMin_calz() {
        return stockMin_calz;
    }

    public void setStockMin_calz(int stockMin_calz) {
        this.stockMin_calz = stockMin_calz;
    }

    public String getColor_calz() {
        return color_calz;
    }

    public void setColor_calz(String color_calz) {
        this.color_calz = color_calz;
    }

    public String getCateg_calz() {
        return categ_calz;
    }

    public void setCateg_calz(String categ_calz) {
        this.categ_calz = categ_calz;
    }

    public Marca getMarca_calz() {
        return marca_calz;
    }

    public void setMarca_calz(Marca marca_calz) {
        this.marca_calz = marca_calz;
    }

    public Modelo getModel_calz() {
        return model_calz;
    }

    public void setModel_calz(Modelo model_calz) {
        this.model_calz = model_calz;
    }

    public TipoCalzado getTipoCalzado_calz() {
        return tipoCalzado_calz;
    }

    public void setTipoCalzado_calz(TipoCalzado tipoCalzado_calz) {
        this.tipoCalzado_calz = tipoCalzado_calz;
    }

}