<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        Marca: <label>${calz.marca_calz}</label><br><br>
        Modelo: <label>${calz.model_calz}</label><br><br>
        Tipo de Calzado: <label>${calz.tipoCalzado_calz}</label><br><br>
        Talla: <label>${calz.talla_calz}</label><br><br>
        Categoria <label>${calz.categ_calz}</label><br><br>
        Precio de Compra: <label>${calz.precioC_calz}</label><br><br>
        Precio de Venta: <label>${calz.precioV_calz}</label><br><br>
        Stock: <label>${calz.stock_calz}</label><br><br>
        <label><a href="ControladorCalzado?r=Obtener&e=editar&id_calz=${calz.id_calz}">Editar</a></label><br><br>
    </body>
</html>
