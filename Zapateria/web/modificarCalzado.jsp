<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px">
            <h2>Modificación de Calzado</h2>
            <form action="ControladorCalzado" method="get">
                <input type="text" name="txtId" value="${calzado.id_calz}" hidden>
            Talla: 
            <select name="txtTalla">
                <option>${calzado.talla_calz}</option>
                <option>32</option>
                <option>34</option>
                <option>36</option>
                <option>38</option>
                <option>40</option>
                <option>42</option>
                <option>44</option>
                <option>46</option>
            </select><br><br>
            Precio de compra: <input type="text" name="txtPrecioC" value="${calzado.precioC_calz}"><br><br>
            Precio de venta: <input type="text" name="txtPrecioV" value="${calzado.precioV_calz}"><br><br>
            Stock mínimo: <input type="text" name="txtStockMin" value="${calzado.stockMin_calz}"><br><br>
            Color: <input type="text" name="txtColor" value="${calzado.color_calz}"><br><br>
            Categoría: 
            <select name="txtCategoria">
                <option>${calzado.categ_calz}</option>
                <option>Hombre</option>
                <option>Mujer</option>
                <option>Niña</option>
                <option>Niño</option>
            </select><br><br>
            Marca: 
            <select name="txtMarca">
                <cal:forEach var="mar" items="${listaMarca}">
                    <cal:if test="${mar.descrip_marca == calzado.marca_calz}">
                        <cal:set var="selected" value="selected"></cal:set>
                    </cal:if>
                    <option ${selected} value="${mar.id_marca}">${mar.descrip_marca}</option>
                </cal:forEach>
            </select>
            <br><br>
            Modelo: 
            <select name="txtModelo">
                <cal:forEach var="mode" items="${listaModelo}">
                    <cal:if test="${mode.descrip_model == calzado.model_calz}">
                        <cal:set var="selected" value="selected"></cal:set>
                    </cal:if>
                    <option ${selected} value="${mode.id_model}">${mode.descrip_model}</option>
                </cal:forEach>
            </select>
            <br><br>
            Tipo de Calzado: 
            <select name="txtTipoCalzado">
                <cal:forEach var="tip" items="${listaTipoCalzado}">
                    <cal:if test="${tip.descrip_tipoCalzado == calzado.tipoCalzado_calz}">
                        <cal:set var="selected" value="selected"></cal:set>
                    </cal:if>
                    <option ${selected} value="${tip.id_tipoCalzado}">${tip.descrip_tipoCalzado}</option>
                </cal:forEach>
            </select>
            <br><br>
            <input class="btn btn-primary" type="submit" name="r" value="Modificar">
        </form>
        </div>
    </body>
</html>
