<%-- 
    Document   : gestionventa
    Created on : 03-ago-2018, 13:54:28
    Author     : USER
--%>
<%@taglib prefix="prov" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <title>Ventas</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Gestión de Ventas</h2>
            <a href="ControladorVenta?r=Nuevo" class="btn btn-primary">Nueva Venta</a><br><br>
            <div class="row">
                <div class="col-sm-8">
                    <div id="yeisonTable" class="table table-hover table-responsive">
                        <table id="tablaOrdenada">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Tipo de Venta</th>
                                    <th>Tipo de Pago</th>
                                    <th>Empleado</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody >
                                <prov:forEach var="vent" items="${listaVenta}">
                                    <tr>
                                        <td>${vent.id_vent}</td>
                                        <td>${vent.fecha_vent}</td>
                                        <td>${vent.total_vent} </td>
                                        <td>${vent.tipo_vent}</td>
                                        <td>${vent.tipoPago_vent}</td>
                                        <td>${vent.emple_vent.id_emp}</td>
                                        <td>${vent.clie_vent.id_cli}</td>
                                        <td>${vent.estado_vent}</td>
                                    </tr>
                                </prov:forEach>
                            </tbody>                                
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script src="js/yeisonTable.js"></script>
    </body>
</html>
