
package Controladores;

import Entidades.Calzado;
import Entidades.Compra;
import Entidades.Dcompra;
import Entidades.Proveedor;
import Modelos.ModeloCalzado;
import Modelos.ModeloCompra;
import Modelos.ModeloDcompra;
import Modelos.ModeloProveedor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControladorCompra", urlPatterns = {"/ControladorCompra"})
public class ControladorCompra extends HttpServlet {
    
    ModeloCompra mc = new ModeloCompra();
    ModeloProveedor mp = new ModeloProveedor();
    ModeloCalzado mca = new ModeloCalzado();
    ModeloDcompra mdc = new ModeloDcompra();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if(r==null ){
            r = "Login";
        }
        switch(r){
            case "Login":
                response.sendRedirect("index.jsp");
                break;
            case "gestioncompra":
                Mostrar(request, response);
                break;
            case "Nuevo":
                Nuevo(request, response);
                break;
            case "Registrar":
                Registrar(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    private void Mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Compra> listaCompra = null;
            listaCompra = mc.listarCompras();
            request.setAttribute("listaCompra", listaCompra);
            request.getRequestDispatcher("gestionCompra.jsp").forward(request, response);
            
        } catch (Exception ex) {
            PrintWriter out = response.getWriter();
            out.print(ex);
        }
        
    }

    private void Nuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Proveedor> listaProveedor = null;
            List<Calzado> listaCalzado = null;
            listaProveedor = mp.listarProveedores();
            listaCalzado = mca.listarCalzados();
            request.setAttribute("listaProveedor", listaProveedor);
            request.setAttribute("listaCalzado", listaCalzado);
            request.getRequestDispatcher("seleccionarCompra.jsp").forward(request, response);
            
        } catch (Exception ex) {
            PrintWriter out = response.getWriter();
            out.print(ex);
        }
    }

    private void Registrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String fecha_comp = request.getParameter("fecha_comp");
            double total_comp = Double.parseDouble(request.getParameter("total_vent"));
            Proveedor prov_comp = new Proveedor();
            prov_comp.setId_prov(Integer.parseInt(request.getParameter("id_prov")));
            
            Compra compra = new Compra(fecha_comp, total_comp, prov_comp);
            mc.insertarCompra(compra);
            
            String[] lista = request.getParameterValues("id_calz");
            for(String id: lista){
                int cant = Integer.parseInt(request.getParameter(id));
                for(int j = 1; j <= cant; j++){
                    Calzado calz_dcomp = new Calzado();
                    calz_dcomp.setId_calz(Integer.parseInt(id));
                    List<Compra> list = mc.listarCompras();
                    Compra v = list.get(list.size()-1);
                    Compra comp_dcomp = new Compra();
                    comp_dcomp.setId_comp(v.getId_comp());
                    Dcompra dcompra = new Dcompra(comp_dcomp, calz_dcomp);
                    mdc.insertarDcompra(dcompra);
                }
            }
            response.sendRedirect("ControladorCompra?r=gestioncompra");
            
        } catch (Exception ex) {
            PrintWriter out = response.getWriter();
            out.print(ex);
        }
    }

}
