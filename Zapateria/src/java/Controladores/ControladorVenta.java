package Controladores;

import Entidades.Calzado;
import Entidades.Cliente;
import Entidades.Dventa;
import Entidades.Empleado;
import Entidades.Venta;
import Modelos.ModeloCalzado;
import Modelos.ModeloCliente;
import Modelos.ModeloDventa;
import Modelos.ModeloProveedor;
import Modelos.ModeloVenta;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControladorVenta", urlPatterns = {"/ControladorVenta"})
public class ControladorVenta extends HttpServlet {

    ModeloVenta mv = new ModeloVenta();
    ModeloCalzado mca = new ModeloCalzado();
    ModeloProveedor mp = new ModeloProveedor();
    ModeloCliente mcl = new ModeloCliente();
    ModeloDventa mdv = new ModeloDventa();
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if(r==null ){
            r = "Login";
        }
        switch(r){
            case "Login":
                response.sendRedirect("index.jsp");break;
            case "gestionventa":
                Mostrar(request, response);break;
            case "Nuevo":
                Nuevo(request, response);break;
            case "Registrar":
                Registrar(request, response);break;
            case "Obtener":
                Obtener(request, response); break;
            case "Modificar":
                Modificar(request, response);break;                
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private void Mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Venta> listaVenta = null;
            listaVenta = mv.listarVentas();
            request.setAttribute("listaVenta", listaVenta);
            request.getRequestDispatcher("gestionVenta.jsp").forward(request, response);
            
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Nuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
        try {
            List<Calzado> listaCalzado = mca.listarCalzados();
            List<Cliente> listaCliente = mcl.listarClientes();
            request.setAttribute("listaCalzado", listaCalzado);
            request.setAttribute("listaCliente", listaCliente);
            request.getRequestDispatcher("seleccionarCalzado.jsp").forward(request, response);
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Registrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String fecha_vent = request.getParameter("fecha_vent");
            double total_vent = Double.parseDouble(request.getParameter("total_vent"));
            String tipo_vent = request.getParameter("tipo_vent");
            String tipoPago_vent = request.getParameter("tipoPago_vent");
            Empleado emple_vent = new Empleado();
            emple_vent.setId_emp(1);
            Cliente clie_vent = new Cliente();
            clie_vent.setId_cli(Integer.parseInt(request.getParameter("id_clie")));
            boolean estado_vent = true;
            
            Venta venta = new Venta(fecha_vent, total_vent, tipo_vent, tipoPago_vent, emple_vent, clie_vent, estado_vent);
            mv.insertarVenta(venta);
            
            String[] lista = request.getParameterValues("id_calz");
            for(String id: lista){
                int cant = Integer.parseInt(request.getParameter(id));
                for(int j = 1; j <= cant; j++){
                    Calzado calz_dvent = new Calzado();
                    calz_dvent.setId_calz(Integer.parseInt(id));
                    List<Venta> list = mv.listarVentas();
                    Venta v = list.get(list.size()-1);
                    Venta vent_dvent = new Venta();
                    vent_dvent.setId_vent(v.getId_vent());
                    Dventa dventa = new Dventa(vent_dvent, calz_dvent);
                    mdv.insertarDventa(dventa);
                }
            }
            response.sendRedirect("ControladorVenta?r=gestionventa");
            
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Obtener(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void Modificar(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
