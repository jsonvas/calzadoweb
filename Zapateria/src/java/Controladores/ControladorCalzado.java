package Controladores;

import Entidades.Calzado;
import Entidades.Marca;
import Entidades.Modelo;
import Entidades.TipoCalzado;
import Modelos.ModeloCalzado;
import Modelos.ModeloMarca;
import Modelos.ModeloModelo;
import Modelos.ModeloTipoCalzado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControladorCalzado", urlPatterns = {"/ControladorCalzado"})
public class ControladorCalzado extends HttpServlet {

    ModeloCalzado mc = new ModeloCalzado();
    
    ModeloMarca mma = new ModeloMarca();
    ModeloModelo mmo = new ModeloModelo();
    ModeloTipoCalzado mtp = new ModeloTipoCalzado();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if(r==null ){
            r = "Login";
        }
        switch(r){
            case "Login":
                response.sendRedirect("index.jsp");break;
            case "gestioncalzado":
                Mostrar(request, response);break;
            case "Nuevo":
                Nuevo(request, response);break;
            case "Registrar":
                Registrar(request, response);break;
            case "Obtener":
                Obtener(request, response); break;
            case "Modificar":
                Modificar(request, response);break;                
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    private void Mostrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
             
        try {
            List<Calzado> listaCalzado = null;
            listaCalzado = mc.listarCalzados();
            request.setAttribute("listaCalzado", listaCalzado);
            request.getRequestDispatcher("gestionCalzado.jsp").forward(request, response);
            
        } catch (Exception ex) {
            PrintWriter out = response.getWriter();
            out.print(ex);
        }
            
    }
    
    private void Nuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Marca> listaMarca = mma.listarMarcas();
            List<Modelo> listaModelo = mmo.listaModelos();
            List<TipoCalzado> listaTipoCalzado = mtp.listaTipoCalzados();
            request.setAttribute("listaMarca", listaMarca);
            request.setAttribute("listaModelo", listaModelo);
            request.setAttribute("listaTipoCalzado", listaTipoCalzado);
            request.getRequestDispatcher("registrarCalzado.jsp").forward(request, response);
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Registrar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        try {
            int talla_calz = Integer.parseInt(request.getParameter("txtTalla"));
            double precioC_calz = Double.parseDouble(request.getParameter("txtPrecioC"));
            double precioV_calz = Double.parseDouble(request.getParameter("txtPrecioV"));
            int stock_calz = 0;
            int stockMin_calz = Integer.parseInt(request.getParameter("txtStockMin"));
            String color_calz = request.getParameter("txtColor");
            String categ_calz = request.getParameter("txtCategoria");
            Marca marca_calz = new Marca();
            marca_calz.setId_marca(Integer.parseInt(request.getParameter("txtMarca")));
            Modelo model_calz = new Modelo();
            model_calz.setId_model(Integer.parseInt(request.getParameter("txtModelo")));
            TipoCalzado tipoCalzado_calz = new TipoCalzado();
            tipoCalzado_calz.setId_tipoCalzado(Integer.parseInt(request.getParameter("txtTipoCalzado")));
            
            Calzado calzado = new Calzado(talla_calz, precioC_calz,precioV_calz,stock_calz, stockMin_calz,color_calz,categ_calz,marca_calz,model_calz, tipoCalzado_calz);
            
            mc.insertarCalzado(calzado);
            
            response.sendRedirect("ControladorCalzado?r=gestioncalzado");
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Obtener(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String e = request.getParameter("e");
            int id_calz = Integer.parseInt(request.getParameter("id_calz"));
            Calzado calzado = new Calzado();
            calzado.setId_calz(id_calz);
            Calzado cal = mc.obtenerCalzado(calzado);
            
            if(e.equals("editar")){
                List<Marca> listaMarca = mma.listarMarcas();
                List<Modelo> listaModelo = mmo.listaModelos();
                List<TipoCalzado> listaTipoCalzado = mtp.listaTipoCalzados();
                request.setAttribute("listaMarca", listaMarca);
                request.setAttribute("listaModelo", listaModelo);
                request.setAttribute("listaTipoCalzado", listaTipoCalzado);
                request.setAttribute("calzado", cal);
                request.getRequestDispatcher("modificarCalzado.jsp").forward(request, response);
            }else if(e.equals("detalle")){
                request.setAttribute("calz", cal);
                request.getRequestDispatcher("detalleCalzado.jsp").forward(request, response);
            }
            
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Modificar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id_calz = Integer.parseInt(request.getParameter("txtId"));
            int talla_calz = Integer.parseInt(request.getParameter("txtTalla"));
            double precioC_calz = Double.parseDouble(request.getParameter("txtPrecioC"));
            double precioV_calz = Double.parseDouble(request.getParameter("txtPrecioV"));
            int stock_calz = 0;
            int stockMin_calz = Integer.parseInt(request.getParameter("txtStockMin"));
            String color_calz = request.getParameter("txtColor");
            String categ_calz = request.getParameter("txtCategoria");
            Marca marca_calz = new Marca();
            marca_calz.setId_marca(Integer.parseInt(request.getParameter("txtMarca")));
            Modelo model_calz = new Modelo();
            model_calz.setId_model(Integer.parseInt(request.getParameter("txtModelo")));
            TipoCalzado tipoCalzado_calz = new TipoCalzado();
            tipoCalzado_calz.setId_tipoCalzado(Integer.parseInt(request.getParameter("txtTipoCalzado")));
            
            Calzado calzado = new Calzado(id_calz, talla_calz, precioC_calz,precioV_calz,stock_calz, stockMin_calz,color_calz,categ_calz,marca_calz,model_calz, tipoCalzado_calz);
            
            mc.modificarCalzado(calzado);
            
            response.sendRedirect("ControladorCalzado?r=gestioncalzado");
            
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }


}
