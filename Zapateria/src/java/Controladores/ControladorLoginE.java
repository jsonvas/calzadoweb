package Controladores;

import Modelos.ModeloCalzado;
import Modelos.ModeloLoginE;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ControladorLoginE", urlPatterns = {"/ControladorLoginE"})
public class ControladorLoginE extends HttpServlet {

    ModeloCalzado mc = new ModeloCalzado();
    ModeloLoginE ml = new ModeloLoginE();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if(r==null ){
            r = "Login";
        }
        switch(r){
            case "Login":
                        response.sendRedirect("index.jsp");break;
            case "Inicio":
                        request.getRequestDispatcher("interno.jsp").forward(request, response);break;
            case "Entrar":
                        Entrar(request, response);break;
            case "Salir":
                        HttpSession session = request.getSession();
                        session.invalidate();
                        response.sendRedirect("index.jsp");break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    private void Entrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession sesionE = request.getSession();
            String user = request.getParameter("user");
            String pass = request.getParameter("pass");
            if(user.equals("") || pass.equals("")){
                request.setAttribute("wrong", "Campos vacíos o datos incorrectos");
                request.getRequestDispatcher("index.jsp").forward(request, response);
            }else{
                boolean log = ml.validarLoginE(user, pass);

                if(log==true){
                    sesionE.setAttribute("user", user);
                    request.getRequestDispatcher("interno.jsp").forward(request, response);
                }else{
                    request.setAttribute("wrong", "Campos vacíos o datos incorrectos");
                    request.getRequestDispatcher("index.jsp").forward(request, response);
                    response.sendRedirect("index.jsp");
                }
            }
                
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
            
        
        
    }

}
