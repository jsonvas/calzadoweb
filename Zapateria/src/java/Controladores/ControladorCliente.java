package Controladores;

import Entidades.Cliente;
import Modelos.ModeloCliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControladorCliente", urlPatterns = {"/ControladorCliente"})
public class ControladorCliente extends HttpServlet {

    ModeloCliente mc = new ModeloCliente();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if(r==null){
            r="Login";
        }
        switch(r){
            case "Login":
                response.sendRedirect("index.jsp");break;
            case "gestioncliente":
                Mostrar(request, response);break;
            case "Nuevo":
                response.sendRedirect("registrarCliente.jsp");break;
            case "Registrar":
                Registrar(request, response);break;
            case "Obtener":
                Obtener(request, response); break;
            case "Modificar":
                Modificar(request, response);break;
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    private void Mostrar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            List<Cliente> listaCliente = mc.listarClientes();
            request.setAttribute("listaCliente", listaCliente);
            request.getRequestDispatcher("gestionCliente.jsp").forward(request, response);
            
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Registrar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String tipodoc_cli = request.getParameter("txtTipoDoc");
            String ndoc_cli = request.getParameter("txtNdoc");
            String nom_cli = request.getParameter("txtNom");
            String apeP_cli = request.getParameter("txtApeP");
            String apeM_cli = request.getParameter("txtApeM");
            String telef_cli = request.getParameter("txtTelef");
            String gen_cli = request.getParameter("txtGen");
            String fechaNac_cli = request.getParameter("txtFechaNac");
            String email_cli = request.getParameter("txtEmail");
            
            Cliente cliente = new Cliente(tipodoc_cli, ndoc_cli, nom_cli, apeP_cli, apeM_cli, telef_cli, gen_cli, fechaNac_cli, email_cli);
            
            mc.insertarCliente(cliente);
            response.sendRedirect("ControladorCliente?r=gestioncliente");
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Obtener(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String e = request.getParameter("e");
            int id_cli = Integer.parseInt(request.getParameter("id_cli"));
            Cliente cliente = new Cliente();
            cliente.setId_cli(id_cli);
            Cliente clie = mc.obtenerCliente(cliente);
            
            if(e.equals("editar")){
                request.setAttribute("cliente", clie);
                request.getRequestDispatcher("modificarCliente.jsp").forward(request, response);
            }
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Modificar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            int id_cli = Integer.parseInt(request.getParameter("txtId"));
            String tipodoc_cli = request.getParameter("txtTipoDoc");
            String ndoc_cli = request.getParameter("txtNdoc");
            String nom_cli = request.getParameter("txtNom");
            String apeP_cli = request.getParameter("txtApeP");
            String apeM_cli = request.getParameter("txtApeM");
            String telef_cli = request.getParameter("txtTelef");
            String gen_cli = request.getParameter("txtGen");
            String fechaNac_cli = request.getParameter("txtFechaNac");
            String email_cli = request.getParameter("txtEmail");
            
            Cliente cliente = new Cliente(id_cli, tipodoc_cli, ndoc_cli, nom_cli, apeP_cli, apeM_cli, telef_cli, gen_cli, fechaNac_cli, email_cli);
            
            mc.modificarCliente(cliente);
            response.sendRedirect("ControladorCliente?r=gestioncliente");
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

}
