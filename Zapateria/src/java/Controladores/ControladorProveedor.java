package Controladores;

import Entidades.Proveedor;
import Modelos.ModeloProveedor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ControladorProveedor", urlPatterns = {"/ControladorProveedor"})
public class ControladorProveedor extends HttpServlet {

    ModeloProveedor mp = new ModeloProveedor();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String r = request.getParameter("r");
        if (r == null) {
            r = "Login";
        }

        switch (r) {
            case "Login":
                response.sendRedirect("index.jsp");break;
            case "gestionproveedor":
                Mostrar(request, response);break;
            case "Nuevo":
                response.sendRedirect("registrarProveedor.jsp");break;
            case "Registrar":
                Registrar(request, response);break;
            case "Obtener":
                Obtener(request, response); break;
            case "Modificar":
                Modificar(request, response);break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    private void Mostrar(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            List<Proveedor> listaProveedores = mp.listarProveedores();
            request.setAttribute("listaProveedor", listaProveedores);
            request.getRequestDispatcher("gestionProveedor.jsp").forward(request, response);
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Registrar(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
        try {
            String nom_prov = request.getParameter("txtNom");
            String tipoDoc_prov = request.getParameter("txtTipoDoc");
            String ndoc_prov = request.getParameter("txtNdoc");
            String telef_prov = request.getParameter("txtTelef");
            String direc_prov = request.getParameter("txtDirec");
            String email_prov = request.getParameter("txtEmail");
            String temp = request.getParameter("txtEstado");
            boolean estado_prov=false;
            if(temp.equals("on")){
                estado_prov=true;
            }
            Proveedor proveedor = new Proveedor(nom_prov, tipoDoc_prov, ndoc_prov, telef_prov, direc_prov, email_prov, estado_prov);
            mp.insertarProveedor(proveedor);
            response.sendRedirect("ControladorProveedor?r=gestionproveedor");
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Obtener(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
        try {
            String e = request.getParameter("e");
            int id_prov = Integer.parseInt(request.getParameter("id_prov"));
            Proveedor proveedor = new Proveedor();
            proveedor.setId_prov(id_prov);
            Proveedor prov = mp.obtenerProveedor(proveedor);
            
            if(e.equals("editar")){
                request.setAttribute("proveedor", prov);
                request.getRequestDispatcher("modificarProveedor.jsp").forward(request, response);
            }
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

    private void Modificar(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
        try {
            int id_prov = Integer.parseInt(request.getParameter("txtId"));
            String nom_prov = request.getParameter("txtNom");
            String tipoDoc_prov = request.getParameter("txtTipoDoc");
            String ndoc_prov = request.getParameter("txtNdoc");
            String telef_prov = request.getParameter("txtTelef");
            String direc_prov = request.getParameter("txtDirec");
            String email_prov = request.getParameter("txtEmail");
            String temp = request.getParameter("txtEstado");
            boolean estado_prov=false;
            if(temp.equals("on")){
                estado_prov=true;
            }
            Proveedor proveedor = new Proveedor(id_prov, nom_prov, tipoDoc_prov, ndoc_prov, telef_prov, direc_prov, email_prov, estado_prov);
            mp.modificarProveedor(proveedor);
            response.sendRedirect("ControladorProveedor?r=gestionproveedor");
        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            out.print(e);
        }
    }

}
