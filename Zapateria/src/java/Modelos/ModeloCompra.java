package Modelos;

import DAO.DAOCompra;
import Entidades.Compra;
import java.util.ArrayList;
import java.util.List;

public class ModeloCompra {
    DAOCompra dao = new DAOCompra();
    
    public List<Compra> listarCompras() throws Exception{
        List<Compra> listaCompra = new ArrayList<>();
        listaCompra = dao.listarCompras();
        return listaCompra;
    }
    public void insertarCompra(Compra compra) throws Exception{
        dao.registrarCompra(compra);
    }
    
    public Compra obtenerCompra(Compra compra) throws Exception{
        Compra comp = new Compra();
        comp = dao.obtenerCompra(compra);
        return comp;
    }
    
    public void modificarCompra(Compra compra) throws Exception{
        dao.modificarCompra(compra);
    }
}
