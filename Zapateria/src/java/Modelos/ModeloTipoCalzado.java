package Modelos;

import DAO.DAOTipoCalzado;
import Entidades.TipoCalzado;
import java.util.List;

public class ModeloTipoCalzado {
    DAOTipoCalzado dao = new DAOTipoCalzado();
    public List<TipoCalzado> listaTipoCalzados() throws Exception{
        List<TipoCalzado> listaTipoCalzados = dao.listarTipoCalzados();
        return listaTipoCalzados;
    }
}
