package Modelos;

import DAO.DAOVenta;
import Entidades.Venta;
import java.util.ArrayList;
import java.util.List;

public class ModeloVenta {
    
    DAOVenta dao = new DAOVenta();
    
    public List<Venta> listarVentas() throws Exception {
        List<Venta> listaVenta = new ArrayList<>();
        listaVenta = dao.listarVentas();
        return listaVenta;
    }
    
    public void insertarVenta(Venta venta)throws Exception {
        dao.registrarVenta(venta);
    }
    
    public Venta obtenerVenta(Venta venta)throws Exception{
        Venta vent = dao.obtenerVenta(venta);
        return vent;
    }
    
    public void modificarVenta(Venta venta)throws Exception {
        dao.modificarVenta(venta);
    }
}
