package Modelos;

import DAO.DAOCalzado;
import Entidades.Calzado;
import java.util.ArrayList;
import java.util.List;

public class ModeloCalzado {
    
    DAOCalzado dao = new DAOCalzado();

    public List<Calzado> listarCalzados() throws Exception {
        List<Calzado> listaCalzado = new ArrayList<>();
        listaCalzado = dao.listarCalzados();
        return listaCalzado;
    }
    
    public void insertarCalzado(Calzado calzado)throws Exception {
        dao.registrarCalzado(calzado);
    }
    public Calzado obtenerCalzado(Calzado calzado)throws Exception{
        Calzado cal = dao.obtenerCalzado(calzado);
        return cal;
    }
    public void modificarCalzado(Calzado calzado)throws Exception {
        dao.modificarCalzado(calzado);
    }
}
