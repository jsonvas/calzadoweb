package Modelos;

import DAO.DAOModelo;
import Entidades.Modelo;
import java.util.List;

public class ModeloModelo {
    DAOModelo dao = new DAOModelo();
    public List<Modelo> listaModelos() throws Exception{
        List<Modelo> listaModelos = dao.listarModelos();
        return listaModelos;
    }
}
