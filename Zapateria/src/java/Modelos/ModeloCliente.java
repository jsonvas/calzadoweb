package Modelos;

import DAO.DAOCliente;
import Entidades.Cliente;
import java.util.ArrayList;
import java.util.List;

public class ModeloCliente {
    
    DAOCliente dao = new DAOCliente();
    
    public List<Cliente> listarClientes() throws Exception{
        List<Cliente> listaCliente = new ArrayList<>();
        listaCliente = dao.listarClientes();
        return listaCliente;
    }
    public void insertarCliente(Cliente cliente)throws Exception {
        dao.registrarCliente(cliente);
    }
    public Cliente obtenerCliente(Cliente cliente)throws Exception{
        Cliente cli = dao.obtenerCliente(cliente);
        return cli;
    }
    public void modificarCliente(Cliente cliente)throws Exception {
        dao.modificarCliente(cliente);
    }
    
}
