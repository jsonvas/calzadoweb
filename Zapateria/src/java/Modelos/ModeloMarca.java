package Modelos;

import DAO.DAOMarca;
import Entidades.Marca;
import java.util.List;

public class ModeloMarca {
    DAOMarca dao = new DAOMarca();
    public List<Marca> listarMarcas() throws Exception{
        List<Marca> listaMarcas = dao.listarMarcas();
        return listaMarcas;
    }
}
