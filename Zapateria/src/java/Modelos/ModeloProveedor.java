package Modelos;

import DAO.DAOProveedor;
import Entidades.Proveedor;
import java.util.List;

public class ModeloProveedor {
    DAOProveedor dao = new DAOProveedor();
    
    public List<Proveedor> listarProveedores() throws Exception{
        List<Proveedor> listaProveedores = dao.listarProveedores();
        return listaProveedores;
    }
    public void insertarProveedor(Proveedor proveedor)throws Exception {
        dao.registrarProveedor(proveedor);
    }
    public Proveedor obtenerProveedor(Proveedor proveedor)throws Exception{
        Proveedor cal = dao.obtenerProveedor(proveedor);
        return cal;
    }
    public void modificarProveedor(Proveedor proveedor)throws Exception {
        dao.modificarProveedor(proveedor);
    }
}
