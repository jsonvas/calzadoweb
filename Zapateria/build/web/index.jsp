<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
            
        <title>Inicie Sesión</title>
    </head>
    <body style="background-image: url('img/ciudad.jpg');background-size: cover;background-attachment: fixed; background-repeat: no-repeat; background-position: center center;">
        <div class="container rounded" style="opacity:0.75  ;align-items: center; width: 400px; margin-top: 150px; background: #343a40; padding: 30px 30px 20px 30px">
            <h2 style="margin-bottom: 25px; color: #f6f6f6">Inicie Sesión</h2>
          <form action="ControladorLoginE" method="get" >
                <div class="form-group" >
                    <input type="text" class="form-control" id="user" autofocus placeholder="Ingrese Usuario" name="user">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="pass" placeholder="Ingrese Contraseña" name="pass">
            </div>
              <div style="margin-top: 20px">
                <input style="display: inline-block;" type="submit" class="btn btn-primary" name="r" value="Entrar">
                <p style="display: inline-block; color: white;">${wrong}</p>  
            </div>
                
          </form>
        </div>    
    </body>
</html>
