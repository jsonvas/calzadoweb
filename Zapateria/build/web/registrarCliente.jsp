<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px">
                
            <h2>Registro de Cliente</h2>
            <form action="ControladorCliente" method="get">
                 <table>
                    <tr>
                        <td>Tipo de de Documento: </td>
                        <td>
                            <input type="radio" name="txtTipoDoc" onclick="hide()" value="RUC" checked>RUC
                            <input type="radio" name="txtTipoDoc" onclick="show()" value="DNI">DNI
                        </td>
                    </tr>
                    <tr>
                        <td>Numero de documento: </td>
                        <td><input type="text" name="txtNdoc" required placeholder="Ingrese documento"></td>
                    </tr>
                    <tr>
                        <td>Nombre: </td>
                        <td><input type="text" name="txtNom" required placeholder="Ingrese nombre"></td>
                    </tr>
                    <tr id="ap" style="display:none">
                        <td>Apellido Paterno</td>
                        <td><input type="text" name="txtApeP" id="p" placeholder="Ingrese Apellido Paterno" value="-" required></td>
                    </tr>
                    <tr id="am" style="display:none">
                        <td>Apellido Materno</td>
                        <td><input  type="text" name="txtApeP" id="m" placeholder="Ingrese Apellido Paterno" value="-" required></td>
                    </tr>
                    <tr>
                        <td>Teléfono: </td>
                        <td><input type="text" name="txtTelef" required placeholder="Ingrese telefono"></td>
                    </tr>
                    <tr id="ge" style="display:none">
                        <td>Género:</td>
                        <td>
                            <input type="radio" name="txtGen" value="M" checked>Masculino
                            <input type="radio" name="txtGen" value="F">Femenino
                        </td>
                    </tr>
                    <tr id="fn" style="display:none">
                        <td>Fecha de Nacimiento: </td>
                        <td><input type="date" name="txtFechaNac" ></td>
                    </tr>
                    <tr >
                        <td>E-mail: </td>
                        <td><input type="text" name="txtEmail" required placeholder="Ingrese E-mail"></td>
                    </tr>
                </table>
            <input class="btn btn-primary" type="submit" name="r" value="Registrar"><br><br>
        </form>
        </div>
        <script type="text/javascript">
            function hide(){
                document.getElementById("ap").style.display = "none"
                document.getElementById("am").style.display = "none"
                document.getElementById("fn").style.display = "none"
                document.getElementById("ge").style.display = "none"
                document.getElementById("p").setAttribute("value","-")
                document.getElementById("m").setAttribute("value","-")
            }
            function show(){
                document.getElementById("ap").style.display = "table-row"
                document.getElementById("p").removeAttribute("value")
                document.getElementById("m").removeAttribute("value")
                document.getElementById("am").style.display = "table-row"
                document.getElementById("fn").style.display = "table-row"
                document.getElementById("ge").style.display = "table-row"
            }
        </script>
    </body>
</html>
