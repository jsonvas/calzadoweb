<%@taglib prefix="comp" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Gestión de Compras</h2>
            <a href="ControladorCompra?r=Nuevo" class="btn btn-primary">Nueva Compra</a><br><br>
            <div class="row">
                <div class="col-sm-8">
                    <div id="yeisonTable" class="table table-hover table-responsive">
                        <table id="tablaOrdenada">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Fecha</th>
                                    <th>Total</th>
                                    <th>Proveedor</th>
                                </tr>
                            </thead>
                            <tbody >
                                <comp:forEach var="comp" items="${listaCompra}">
                                    <tr>
                                        <td>${comp.id_comp}</td>
                                        <td>${comp.fecha_comp}</td>
                                        <td>${comp.total_comp} </td>
                                        <td>${comp.prov_comp.id_prov}</td>
                                    </tr>
                                </comp:forEach>
                            </tbody>                                
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <script src="js/yeisonTable.js"></script>
    </body>
</html>
