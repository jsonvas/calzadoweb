<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px">
            <h2>Registro de Proveedor</h2>
            <form action="ControladorProveedor" method="get">
                Nombre: <input type="text" name="txtNom" autofocus required><br><br>
            Tipo de de Documento: 
            <input type="radio" name="txtTipoDoc" value="RUC" checked >RUC
            <input type="radio" name="txtTipoDoc" value="DNI">DNI<br><br>
            Nº Documento: <input type="text" name="txtNdoc" required><br><br>
            Telefono:  <input type="text" name="txtTelef" required><br><br>
            Dirección: <input type="text" name="txtDirec" required><br><br>
            E-mail: <input type="text" name="txtEmail" required><br><br>
            Estado: <input type="checkbox" name="txtEstado" checked>Vigente<br><br>
            <input class="btn btn-primary" type="submit" name="r" value="Registrar">
        </form>
        </div>
        
    </body>
</html>
