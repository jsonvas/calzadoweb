<%@taglib prefix="cli" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>Clientes</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Gestión de Clientes</h2>
            <a href="ControladorCliente?r=Nuevo" class="btn btn-primary">Nuevo Cliente</a><br><br>
            <div class="row">
                <div class="col-sm-8">
                    <div id="yeisonTable" class="table table-hover table-responsive">
                        <table id="tablaOrdenada">
                            <head>
                                <tr>
                                    <th>ID</th>
                                    <th>Tipo de Documento</th>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Genero</th>
                                    <th>Telefono</th>
                                    <th>Email</th>
                                </tr>
                            </head>
                            <tbody>
                                <cli:forEach var="cli" items="${listaCliente}">
                                    <tr>
                                        <td>${cli.id_cli}</td>
                                        <td>${cli.tipodoc_cli}</td>
                                        <td>${cli.ndoc_cli}</td>
                                        <td>${cli.nom_cli} </td>
                                        <td>${cli.apeP_cli} ${cli.apeM_cli}</td>
                                        <td>${cli.fechaNac_cli}</td>
                                        <td>${cli.gen_cli}</td>
                                        <td>${cli.telef_cli}</td>
                                        <td>${cli.email_cli}</td>
                                        <td><a href="ControladorCliente?r=Obtener&e=editar&id_cli=${cli.id_cli}" >Editar</a></td>
                                    </tr>
                                </cli:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
        </div>
        <script src="js/yeisonTable.js"></script>
    </body>
</html>
