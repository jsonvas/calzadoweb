<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">       
        <title>Calzados</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Gestión de Calzados</h2>
            <a href="ControladorCalzado?r=Nuevo" class="btn btn-primary">Nuevo Calzado</a><br><br>
            <div class="row">
                <div class="col-sm-8">
                    <div id="yeisonTable" class="table table-hover table-responsive">
                        <table id="tablaOrdenada">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Tipo de Calzado</th>
                                    <th>Talla</th>
                                    <th>Categoria</th>
                                    <th>Precio de Compra</th>
                                    <th>Precio de Venta</th>
                                    <th>Stock</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tbody>
                                <cal:forEach var="calz" items="${listaCalzado}">
                                    <tr>
                                        <td>${calz.id_calz}</td>
                                        <td>${calz.marca_calz.descrip_marca}</td>
                                        <td>${calz.model_calz.descrip_model}</td>
                                        <td>${calz.tipoCalzado_calz.descrip_tipoCalzado}</td>
                                        <td>${calz.talla_calz}</td>
                                        <td>${calz.categ_calz}</td>
                                        <td>${calz.precioC_calz}</td>
                                        <td>${calz.precioV_calz}</td>
                                        <td>${calz.stock_calz}</td>
                                        <td><a href="ControladorCalzado?r=Obtener&e=editar&id_calz=${calz.id_calz}">Editar</a></td>
                                    </tr>
                                </cal:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/yeisonTable.js">
        </script>
        
    </body>
</html>
