                        
//            --------------Localizacion de elementos de tabla-----------------------
            let yeisonTable = document.getElementById("yeisonTable")
            let tableOrdenada = document.getElementById("tablaOrdenada")
            tableOrdenada.setAttribute("class", "table table-responsive")
            let th = tableOrdenada.firstElementChild
            let tb = th.nextElementSibling
            let ths = th.getElementsByTagName("th")
            let trs = tb.getElementsByTagName("tr")
            let tds = trs[0].getElementsByTagName("td")
                        
//            -------------------Creacion de elemento filtro de numero de elemntos por pagina--------------------------
            let filter_cant = document.createElement("div")
            filter_cant.setAttribute("style","display:inline-flex")
            let label = document.createElement("label")
            let select = document.createElement("select")
            select.setAttribute("onchange","cantPag()")
            select.setAttribute("id","cantPag")
            
            let option5 = document.createElement("option")
            option5.value = 5
            option5.setAttribute("selected","true")
            let option10 = document.createElement("option")
            option10.value= 10
            let option20 = document.createElement("option")
            option20.value = 20
            let option50 = document.createElement("option")
            option50.value = 50
            
            let op5 = document.createTextNode("5")
            let op10 = document.createTextNode("10")
            let op20 = document.createTextNode("20")
            let op50 = document.createTextNode("50")
            
            option5.appendChild(op5)
            option10.appendChild(op10)
            option20.appendChild(op20)
            option50.appendChild(op50)
            
            select.appendChild(option5)
            select.appendChild(option10)
            select.appendChild(option20)
            select.appendChild(option50)
            
            let text = document.createTextNode(" Registros por Pagina")
            
            label.appendChild(select)
            label.appendChild(text)
            
            filter_cant.appendChild(label)
            
//            -------------------Creacion de elemento de filtro de busqueda por caracter--------------------------
            let filter_name = document.createElement("div")
            filter_name.setAttribute("style","display:inline-flex")
            filter_name.setAttribute("style","float:right")
            let label2 = label.cloneNode(false)
            let input = document.createElement("input")
            input.setAttribute("type", "text")
            input.setAttribute("name","filtro")
            input.setAttribute("id","filtro")
            input.setAttribute("onkeyup", "search()")
            
            let text2 = document.createTextNode("Buscar: ")
            
            label2.appendChild(text2)
            label2.appendChild(input)
            
            filter_name.appendChild(label2)
            
//            -------------------Insercion de elementos de Filtro --------------------------
            let cab = document.createElement("div")
            cab.appendChild(filter_cant)
            cab.appendChild(filter_name)
            yeisonTable.insertBefore(cab, tableOrdenada)
                       
//            ------------------------Creacion de array para de elementos de tabla---------------------------
            let att = []
            let listaTabla = []
            let objetosTabla = []
            let newList = []
            
//            ---------------------- Creacion de nombres de objetos --------------------------------------
            for(let i = 0; i < ths.length; i++){
                att[i] = ths[i].textContent.replace(/\s+/gi,"")
            }
            
//            -----------------------Asignacion de nombre e insercion de cadenas y objetos de tabla ---------------------
            for(let j = 0; j < trs.length; j++){
                let calzado = {}
                let objeto = {}
                tds = trs[j].getElementsByTagName("td")
                for(let i = 0; i < tds.length; i++){
                    calzado[att[i]] = tds[i].textContent
                    objeto[att[i]] = tds[i]
                }
                listaTabla.push(calzado)
                objetosTabla.push(objeto)
            }
            
            search()
//            ---------------------- Busqueda por caracter ---------------------------
            function search(){
                let word = document.getElementById("filtro").value
                if(word != "" && word != null){
                    newList = listaTabla.filter(ele => Object.values(ele).some(e => e.includes(word)))
                }else{
                    newList = listaTabla.slice(0)
                }  
                cantPag()
            }
            
//            ---------------------- Cantidad de elemntos por pagina--------------
            function cantPag(){
                let select = document.getElementById("cantPag")
                let cant = select.options[select.selectedIndex].value
                page(cant, 1)
            }
            
//            ------------------------ Paginacion --------------------------------                                   
            function page(cant, pag){
                let pager
                if(document.getElementById("pag") === null){
                    pager = document.createElement("div")
                    pager.setAttribute("style","display:inline-flex; float:right")
                    pager.setAttribute("id","pag")
                }else{
                    pager = document.getElementById("pag")
                    pager.innerHTML = ""
                }
                
                let ul = document.createElement("ul")
                ul.setAttribute("class","pagination")

                let li_back = document.createElement("li")
                li_back.setAttribute("class","page-item")
                let a_back = document.createElement("a")
                a_back.setAttribute("class","page-link")
                let text_back = document.createTextNode("Atras")

                a_back.setAttribute("href","#")
                if(pag > 1){
                    a_back.setAttribute("onclick","page("+cant+","+(pag-1)+")")
                } 
                if(pag == 1){
                    li_back.setAttribute("class","page-item disabled")                    
                }
                a_back.appendChild(text_back)
                li_back.appendChild(a_back)
                

                let li_next = document.createElement("li")
                li_next.setAttribute("class","page-item")
                if(cant*pag >= newList.length){
                    li_next.setAttribute("class","page-item disabled")
                }
                let a_next = document.createElement("a")
                a_next.setAttribute("class","page-link")
                let text_next = document.createTextNode("Siguiente")

                a_next.setAttribute("href","#")
                a_next.setAttribute("onclick","page("+cant+","+(pag+1)+")")
                a_next.appendChild(text_next)            
                li_next.appendChild(a_next)

                ul.appendChild(li_back)
                ul.appendChild(li_next)
                
                for(let i = 0,c = 1; i < newList.length; i = i+cant){
                    li = document.createElement("li")
                    li.setAttribute("class","page-item")
                    if(pag == c){
                        li.setAttribute("class","page-item active")
                    }
                    let a = document.createElement("a")
                    a.setAttribute("href","#")
                    a.setAttribute("class","page-link")
                    a.setAttribute("onclick","page("+cant+","+c+")")

                    let txt = document.createTextNode(c)
                    li.appendChild(a)
                    a.appendChild(txt)
                    ul.insertBefore(li,li_next)
                    c++
                }
                
                let pie = document.createElement("div")
                pager.appendChild(ul)
                pie.appendChild(pager)
                yeisonTable.appendChild(pie)
                
                view(cant, pag)
            }
            
//            --------------------------- Vistas de elementos------------------------------------            
            function view(cant, pag){
                let lista = []

                let list = []
                for(let obj of newList){
                    for(let i= 0; i < objetosTabla.length; i++){
                        if(objetosTabla[i]["ID"].textContent == obj["ID"]){
                        list.push(objetosTabla[i])
                        }
                    }
                }
                lista = list.slice((pag-1)*cant, pag*cant)

                tb.innerHTML = ""
                if(lista.length > 0){
                    for(let ele of lista){
                        let raw = tb.insertRow(-1)
                        for(let i = 0; i < ths.length; i++){
                            raw.appendChild(ele[att[i]])
                        }
                    }
                }
                
                info(cant, pag)
            }
            
//            -------------------- Informacion de vistas--------------------------------
            function info(cant,pag){
                let filter_info
                if(document.getElementById("info") === null){
                    filter_info = document.createElement("div")
                    filter_info.setAttribute("style","display:inline-flex; width:50%; align-content:center")
                    filter_info.setAttribute("id","info")
                }else{
                    filter_info = document.getElementById("info")
                    filter_info.innerHTML = ""
                }
                                
                let start = ((pag-1)*cant+1)
                                
                let end = pag*cant
                
                if(end > newList.length){
                    end = newList.length
                }
                if(end == 0){
                    start = 0
                }
                
                let info = document.createTextNode("Mostrando "+ start +" - "+ end + " de " + newList.length + " Registros")

                filter_info.appendChild(info)
                if(newList != undefined){
                    let extra = document.createTextNode(" (Filtrado de "+listaTabla.length+" elementos)")
                    filter_info.appendChild(extra)
                }
                
                let p = document.getElementById("pag")
                let d = yeisonTable.lastChild
                d.insertBefore(filter_info, p)
            }
            

