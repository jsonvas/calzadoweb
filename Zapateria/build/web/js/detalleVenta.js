
            let fondo = document.getElementById("fondo")
            let ventana = document.getElementById("ventana")
            let listaVenta = []
            
//          ------------FECHA-----------------------
            let date = new Date()
            let d = date.getDay() 
            d<10?d="0"+d:d=d
            let m = date.getMonth()
            m<10?m="0"+m:m=m
            let y = date.getFullYear()
            let fecha = y+"-"+m+"-"+ d
            document.getElementById("fecha").setAttribute("value",fecha)
            document.getElementById("fecha").setAttribute("readonly","true")
            document.getElementById("fecha").setAttribute("style","width: 115px")
            
            if(listaVenta.length < 1){
                let table = document.getElementById("detalle")
                    let e = document.createTextNode("Empieze agregando productos.")
                    table.appendChild(e)
                    document.getElementById("r").setAttribute("disabled", "true")
            }
                   
            function cargar(){
                let table = document.getElementById("detalle")
                table.innerHTML=""
                let total = 0;
                if(listaVenta.length > 0){
                    for(let calzado of listaVenta){
                        total = total + calzado.getTotal()
                        let table = document.getElementById("detalle")
                        let tr = table.insertRow(-1)
                        
                        let td1 = tr.insertCell(0)
                        let td2 = tr.insertCell(1)
                        let td3 = tr.insertCell(2)
                        let td4 = tr.insertCell(3)
                        let td5 = tr.insertCell(4)
                                                              
                        let e2 = document.createElement("input")
                        e2.setAttribute("type", "text")
                        e2.setAttribute("onchange", "cambio("+calzado.getId()+")")
                        e2.style.width="50px"
                        e2.setAttribute("name",calzado.getId())
                        e2.setAttribute("id",calzado.getId())
                        e2.setAttribute("value",calzado.getCant())                

                        let e4 = document.createTextNode(calzado.getMarca() + " " + calzado.getModel())

                        let e5 = document.createElement("input")
                        e5.setAttribute("type", "text")
                        e5.setAttribute("value",calzado.getPrec())
                        e5.setAttribute("readonly","true")

                        let e6 = document.createElement("input")
                        e6.setAttribute("type", "text")
                        e6.setAttribute("id","_"+calzado.getId())
                        e6.setAttribute("value", calzado.getTotal())
                        e6.setAttribute("readonly","true")

                        let e7 = document.createElement("input")
                        e7.setAttribute("type", "button")
                        e7.setAttribute("value", "X")
                        e7.setAttribute("onclick", "remove("+calzado.getId()+")")
                        e7.setAttribute("class", "btn btn-danger")
                        
                        let e8 = document.createElement("input")
                        e8.setAttribute("type", "checkbox")
                        e8.setAttribute("checked","true")
                        e8.setAttribute("name","id_calz")
                        e8.setAttribute("hidden","true")
                        e8.setAttribute("value",calzado.getId())

                        td1.appendChild(e8)
                        td1.appendChild(e2)
                        td2.appendChild(e4)
                        td3.appendChild(e5)
                        td4.appendChild(e6)
                        td5.appendChild(e7)
                        
                    }
                    let tr = table.insertRow(-1)
                    let td = tr.insertCell(0);
                    let e = document.createTextNode("Total: S/.")
                    let tt = document.createElement("input")
                    tt.setAttribute("type", "text")
                    tt.setAttribute("name", "total_vent")
                    tt.setAttribute("readonly", "true")
                    tt.setAttribute("value",total)
                    
                    td.appendChild(e)
                    td.appendChild(tt)
                    td.setAttribute("colspan","5")
                    
                    document.getElementById("r").removeAttribute("disabled")
                }else{
                    let e = document.createTextNode("Empieze agregando productos.")
                    table.appendChild(e)
                    document.getElementById("r").setAttribute("disabled", "true")
                }
            }
            class Calzado{
                constructor(id, cant, prec, model, marca){
                    this.id = id
                    this.cant = cant
                    this.prec = prec
                    this.total = prec * cant
                    this.model = model
                    this.marca = marca
                }
                
                getId(){
                    return this.id
                }
                setId(newId){
                    this.id = newId
                }
                getCant(){
                    return this.cant
                }
                setCant(newCant){
                    this.cant = newCant
                }
                getPrec(){
                    return this.prec
                }
                setPrec(newPrec){
                    this.prec = newPrec
                }
                getTotal(){
                    return this.total
                }
                setTotal(newTotal){
                    this.total = newTotal
                }
                getModel(){
                    return this.model
                }
                setModel(newModel){
                    this.model = newModel
                }
                getMarca(){
                    return this.marca
                }
                setMarca(newMarca){
                    this.marca = newMarca
                }
            }
            
            function modalEntrar(){
                fondo.style.display = "block"
                ventana.style.display = "block"
            }
            
            function modalSalir(){
                fondo.style.display = "none"
                ventana.style.display = "none"
            }
            
            function add(id,cant,marca,model,precio){
                if(listaVenta.length < 1){
                    let calzado = new Calzado(id,cant,precio, model, marca);
                    listaVenta.push(calzado)
                }else if(listaVenta.length > 0){
                    let a = true;
                    for(let calz of listaVenta){
                        if(calz.id == id){
                            calz.cant = calz.getCant()+1
                            calz.total = calz.getCant() * calz.getPrec()
                            a = false
                            break;
                        }
                    }
                    if(a){
                        let calzado = new Calzado(id,cant,precio, model, marca);
                        listaVenta.push(calzado) 
                    }
                }                
                cargar()
            }
            
            function cambio(id){
                let cant = document.getElementById(id).value
                listaVenta.map(calz => {
                    if(calz.id == id){
                        calz.cant = parseInt(cant)
                        calz.total = parseInt(cant) * calz.getPrec()
                    }
                })
                cargar()
            }
            
            function remove(id){
                for(let calz of listaVenta){
                    if(calz.id == id){
                        listaVenta.splice(listaVenta.indexOf(calz), 1)
                        break
                    }
                }   
                cargar()
            }
            