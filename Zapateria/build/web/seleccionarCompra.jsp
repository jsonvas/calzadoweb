<%@taglib prefix="cal" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Nueva Compra</h2>
            <div class="row">
                <div class="col-sm-10">
                    <form action="ControladorCompra" method="get">                
                        Seleccione Proveedor: 
                        <select name="id_prov">
                            <cal:forEach var="prov" items="${listaProveedor}">
                                <option value="${prov.id_prov}">${prov.nom_prov}</option>
                            </cal:forEach>
                        </select>
                        <input type="text" id="fecha" name="fecha_comp" value=""><br><br>

                        <table class="table table-condensed table-responsive">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Descripcion</th>
                                    <th>P.Unitario</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id="detalle" onload="cargar()">
                            </tbody>
                        </table>
                        <input type="button" onclick="modalEntrar(); modalEntrar()" class="btn btn-primary" value="Agregar Productos">
                        <input class="btn btn-success" type="submit" name="r" id="r" value="Registrar">
                    </form>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
            
        </div>
        <div id="fondo" >
            <div id="ventana" onload="document.getElementById('ventana').focus()" onblur="modalSalir()">
                <button style="float: right; border-radius: 5px; border: none; margin-bottom: 10px"   class=" btn-md" onclick="modalSalir()" >X</button>
                
                <div id="yeisonTable" class="table table-hover table-responsive">
                    <table id="tablaOrdenada">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Tipo de Calzado</th>
                                    <th>Talla</th>
                                    <th>Categoria</th>
                                    <th>Precio de Compra</th>
                                    <th>Precio de Venta</th>
                                    <th>Stock</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <cal:forEach var="calz" items="${listaCalzado}">
                                    <tr>
                                        <td>${calz.id_calz}</td>
                                        <td>${calz.marca_calz.descrip_marca}</td>
                                        <td>${calz.model_calz.descrip_model}</td>
                                        <td>${calz.tipoCalzado_calz.descrip_tipoCalzado}</td>
                                        <td>${calz.talla_calz}</td>
                                        <td>${calz.categ_calz}</td>
                                        <td>${calz.precioC_calz}</td>
                                        <td>${calz.precioV_calz}</td>
                                        <td>${calz.stock_calz}</td>
                                        <td><button onclick="add(${calz.id_calz}, 1,'${calz.marca_calz.descrip_marca}','${calz.model_calz.descrip_model}',${calz.precioV_calz})" class="btn btn-success">Agregar</button></td>
                                    </tr>
                                </cal:forEach>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/detalleVenta.js"></script>
        <script type="text/javascript" src="js/yeisonTable.js"></script>
    </body>
</html>
