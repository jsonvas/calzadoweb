<%@taglib prefix="prov" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>Proveedores</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px;">
            <h2>Gestión de Proveedores</h2>
            <a href="ControladorProveedor?r=Nuevo" class="btn btn-primary">Nuevo Proveedor</a><br><br>
            <div class="row">
                <div class="col-sm-8">
                    <div id="yeisonTable" class="table table-hover table-responsive">
                        <table id="tablaOrdenada">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tipo de Documento</th>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th>Telefono</th>
                                    <th>Dirección</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <prov:forEach var="prov" items="${listaProveedor}">
                                    <tr>
                                        <td>${prov.id_prov}</td>
                                        <td>${prov.tipdoc_prov}</td>
                                        <td>${prov.ndoc_prov}</td>
                                        <td>${prov.nom_prov} </td>
                                        <td>${prov.telef_prov}</td>
                                        <td>${prov.direc_prov}</td>
                                        <td>${prov.email_prov}</td>
                                        <td><a href="ControladorProveedor?r=Obtener&e=editar&id_prov=${prov.id_prov}">Editar</a></td>
                                    </tr>
                                </prov:forEach>
                            </tbody>    
                        </table>
                    </div>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
        </div>
        <script src="js/yeisonTable.js"></script>
    </body>
</html>
