<%@taglib prefix="cli" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px">
            <h2>Modificación de Cliente</h2>
            <form action="ControladorCliente" method="get">
                <input type="text" name="txtId" value="${cliente.id_cli}" hidden>
            Tipo de de Documento: 
                <cli:if test="${cliente.tipodoc_cli=='RUC'}">
                    <input type="radio" name="txtTipoDoc" value="RUC" checked>RUC
                    <input type="radio" name="txtTipoDoc" value="DNI">DNI<br><br>
                </cli:if>
                <cli:if test="${cliente.tipodoc_cli=='DNI'}">
                    <input type="radio" name="txtTipoDoc" value="RUC" >RUC
                    <input type="radio" name="txtTipoDoc" value="DNI" checked>DNI<br><br>
                </cli:if>
                Nº Documento: <input type="text" name="txtNdoc" value="${cliente.ndoc_cli}" required><br><br>
                Nombre: <input type="text" name="txtNom" value="${cliente.nom_cli}" required><br><br>
                Apellido Paterno: <input type="text" name="txtApeP" value="${cliente.apeP_cli}" required><br><br>
                Apellido Materno:  <input type="text" name="txtApeM" value="${cliente.apeM_cli}" required><br><br>
                Telefono:  <input type="text" name="txtTelef" value="${cliente.telef_cli}" required><br><br>
                Genero: 
                <cli:if test="${cliente.gen_cli=='M'}">
                    <input type="radio" name="txtTipoDoc" value="M" checked>Masculino
                    <input type="radio" name="txtTipoDoc" value="F">Femenino<br><br>
                </cli:if>
                <cli:if test="${cliente.gen_cli=='F'}">
                    <input type="radio" name="txtTipoDoc" value="M" >Masculino
                    <input type="radio" name="txtTipoDoc" value="F" checked>Femenino<br><br>
                </cli:if>
                Fecha de Nacimiento:  <input type="date" name="txtFechaNac" ><br><br>
                E-mail: <input type="text" name="txtEmail" value="${cliente.email_cli}" required><br><br>
                <input class="btn btn-primary" type="submit" name="r" value="Modificar"><br><br>
            </form>
        </div>
    </body>
</html>
