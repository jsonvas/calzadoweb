<%@taglib prefix="prov" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="navInterno.jsp"%>
        <div class="container-fluid" style="margin-top: 80px">
            <h2>Modificar de Proveedor</h2>
            <form action="ControladorProveedor" method="get">
                <input type="text" name="txtId" value="${proveedor.id_prov}" hidden>
                Nombre: <input type="text" name="txtNom" value="${proveedor.nom_prov}" autofocus><br><br>
                Tipo de de Documento: 
                <prov:if test="${proveedor.tipdoc_prov=='RUC'}">
                    <input type="radio" name="txtTipoDoc" value="RUC" checked>RUC
                    <input type="radio" name="txtTipoDoc" value="DNI">DNI<br><br>
                </prov:if>
                <prov:if test="${proveedor.tipdoc_prov=='DNI'}">
                    <input type="radio" name="txtTipoDoc" value="RUC" >RUC
                    <input type="radio" name="txtTipoDoc" value="DNI" checked>DNI<br><br>
                </prov:if>
                
                Nº Documento: <input type="text" name="txtNdoc" value="${proveedor.ndoc_prov}"><br><br>
                Telefono:  <input type="text" name="txtTelef" value="${proveedor.telef_prov}"><br><br>
                Dirección: <input type="text" name="txtDirec" value="${proveedor.direc_prov}"><br><br>
                E-mail: <input type="text" name="txtEmail" value="${proveedor.email_prov}"><br><br>
                <prov:if test="${proveedor.estado_prov==true}">
                    Estado: <input type="checkbox" name="txtEstado" checked>Vigente<br><br>
                </prov:if>
                <prov:if test="${proveedor.estado_prov==false}">
                    Estado: <input type="checkbox" name="txtEstado">Vigente<br><br>
                </prov:if>
                <input class="btn btn-primary" type="submit" name="r" value="Modificar">
            </form>
        </div>
    </body>
</html>
